# -*- coding: utf-8 -*-
"""
/***************************************************************************
 EnergyHub
                                 A QGIS plugin
 QGIS plugin for HUES platform, eHub Model
                             -------------------
        begin                : 2017-06-17
        copyright            : (C) 2017 by Rachit Kansal
        email                : rachitkansal1995@gmail.com
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load EnergyHub class from file EnergyHub.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .energy_hub import EnergyHub
    return EnergyHub(iface)
