# -*- coding: utf-8 -*-
"""
/***************************************************************************
 EnergyHub
                                 A QGIS plugin
 QGIS plugin for HUES platform, eHub Model
                             -----------------------------
        begin                : 2017-06-17
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Rachit Kansal
        email                : rachitkansal1995@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# PyQT libraries
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication, \
    Qt, SIGNAL, QObject, QVariant
from PyQt4.QtGui import QAction, QIcon, QVBoxLayout, QCheckBox, QFileDialog

# QGIS libraries
from qgis.core import QgsMapLayerRegistry, QgsVectorLayer, QgsFeature, \
    QgsGeometry, QgsPoint, QgsField, QgsExpression
from qgis.gui import QgsMapLayerProxyModel, QgsMapToolEmitPoint, \
    QgsVertexMarker

# QGIS Network Analysis Library
from qgis.networkanalysis import QgsLineVectorLayerDirector,\
    QgsDistanceArcProperter, QgsGraphBuilder, QgsGraphAnalyzer

# Library for PyQt Siganls
from functools import partial

# Initialize Qt resources from file resources.py
import resources

# Importing for library for transforming the projections
from pyproj import Proj, transform

# Import the code for main dialog
from energy_hub_dialog import EnergyHubDialog

# Import the code for the add, remove  dialog boxes of TAB1
from add_dialog_tab1 import AddDialogTab1
from remove_dialog_tab1 import RemoveDialogTab1

# Import the code for the add, remove and edit dialog boxes of TAB2
from add_dialog_tab2 import AddDialogTab2
from edit_dialog_tab2 import EditDialogTab2
from remove_dialog_tab2 import RemoveDialogTab2

# Import the code for the add, remove and edit dialog boxes of TAB3
from edit_dialog_tab3 import EditDialogTab3

# Import Custom UI elements from utils
from utils import LabelEdit

# Import for handling JSON, CSV and Excel files
from pandas import ExcelFile
import xlsxwriter
import json
import csv

# Importing the libraray used ffor creating depp copy
import copy

# Library to import and work with the GUI elements
from PyQt4 import uic

# OS module to handle relative paths
import os.path

# Importing Math Library for calculation of distances
from math import sin, cos, sqrt, atan2, radians

# Importing the Processing toolbox to use the GRASS Algorithms
import processing


class EnergyHub:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'EnergyHub_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = EnergyHubDialog()

        # Tab flags for warning message purposes
        self.tab1_flag = 0
        self.tab2_flag = 0
        self.tab3_flag = 0

        # Initialize the Add, Remove and Edit Dialog Boxes of TAB1
        # (after translation) and keep reference
        self.add_dlg_tab1 = AddDialogTab1()
        self.remove_dlg_tab1 = RemoveDialogTab1()

        # Initialize the Add, Remove and Edit Dialog Boxes of TAB2
        # (after translation) and keep reference
        self.add_dlg_tab2 = AddDialogTab2()
        self.remove_dlg_tab2 = RemoveDialogTab2()
        self.edit_dlg_tab2 = EditDialogTab2()

        # Initialize the Add, Remove and Clear Dialog Boxes of TAB3
        # (after translation) and keep reference
        self.edit_dlg_tab3 = EditDialogTab3()

        # Technologies CSV filename
        self.filename_technologies = "metadata_technologies.json"

        # Technology output CSV filename
        self.filename_output = "metadata_outputs.json"

        # Loading the GUI for the dockwidget of Tab3
        self.dock_tab3_1 = uic.loadUi(os.path.join(self.plugin_dir,
                                                   "hubs_dock.ui"))

        # Loading the GUI for the dockwidget of Tab4
        self.dock_tab4_1 = uic.loadUi(os.path.join(self.plugin_dir,
                                                   "connection_dock_1.ui"))

        # Setting ScrollArea policies for both the scrollAreas for TAB1
        self.dlg.exportPriceList.setVerticalScrollBarPolicy(
            Qt.ScrollBarAlwaysOn)
        self.dlg.exportPriceList.setHorizontalScrollBarPolicy(
            Qt.ScrollBarAlwaysOff)
        self.dlg.exportPriceList.setWidgetResizable(True)
        self.dlg.exportPriceList.setAlignment(Qt.AlignTop)

        # Setting ScrollArea policies for both the scrollAreas for TAB2
        self.dlg.techParamList.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.dlg.techParamList.setHorizontalScrollBarPolicy(
            Qt.ScrollBarAlwaysOff)
        self.dlg.techParamList.setWidgetResizable(True)
        self.dlg.techParamList.setAlignment(Qt.AlignTop)

        self.dlg.outputsParamList.setVerticalScrollBarPolicy(
            Qt.ScrollBarAlwaysOn)
        self.dlg.outputsParamList.setHorizontalScrollBarPolicy(
            Qt.ScrollBarAlwaysOff)
        self.dlg.outputsParamList.setWidgetResizable(True)
        self.dlg.outputsParamList.setAlignment(Qt.AlignTop)

        # self.dlg.carbonFactorsList.setVerticalScrollBarPolicy(
        #     Qt.ScrollBarAlwaysOn)
        # self.dlg.carbonFactorsList.setHorizontalScrollBarPolicy(
        #     Qt.ScrollBarAlwaysOff)
        # self.dlg.carbonFactorsList.setWidgetResizable(True)
        # self.dlg.carbonFactorsList.setAlignment(Qt.AlignTop)

        # Setting ScrollArea policies for TAB3 Storage List
        self.dlg.storageListTab3.setVerticalScrollBarPolicy(
            Qt.ScrollBarAlwaysOn)
        self.dlg.storageListTab3.setHorizontalScrollBarPolicy(
            Qt.ScrollBarAlwaysOff)
        self.dlg.storageListTab3.setWidgetResizable(True)
        self.dlg.storageListTab3.setAlignment(Qt.AlignTop)

        # Setting ScrollArea policies for both the scrollAreas for Dock of TAB3
        self.dock_tab3_1.scrollArea1_1Dock.setVerticalScrollBarPolicy(
            Qt.ScrollBarAlwaysOn)
        self.dock_tab3_1.scrollArea1_1Dock.setHorizontalScrollBarPolicy(
            Qt.ScrollBarAlwaysOff)
        self.dock_tab3_1.scrollArea1_1Dock.setWidgetResizable(True)
        self.dock_tab3_1.scrollArea1_1Dock.setAlignment(Qt.AlignTop)

        self.dock_tab3_1.scrollArea2_1Dock.setVerticalScrollBarPolicy(
            Qt.ScrollBarAlwaysOn)
        self.dock_tab3_1.scrollArea2_1Dock.setHorizontalScrollBarPolicy(
            Qt.ScrollBarAlwaysOff)
        self.dock_tab3_1.scrollArea2_1Dock.setWidgetResizable(True)
        self.dock_tab3_1.scrollArea2_1Dock.setAlignment(Qt.AlignTop)

        # Initializing the clicking tool for the button on tab4
        self.clickTool = QgsMapToolEmitPoint(self.iface.mapCanvas())

        # Flag to determine if the road layer was used of the road layer
        self.network_flag = 0

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&Energy Hub And Network')

        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'EnergyHub')
        self.toolbar.setObjectName(u'EnergyHub')

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('EnergyHub', message)

    def add_action(
            self,
            icon_path,
            text,
            callback,
            enabled_flag=True,
            add_to_menu=True,
            add_to_toolbar=True,
            status_tip=None,
            whats_this=None,
            parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """
        # Create the dialog (after translation) and keep reference
        self.dlg = EnergyHubDialog()

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""
        icon_path = ':/plugins/EnergyHub/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Energy Hub And Network'),
            callback=self.run,
            parent=self.iface.mainWindow())
        self.initializeGUI()

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&Energy Hub And Network'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

    def updateComboBox(self):
        """Testing for the presence of layers
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Polygon Layer (Buildings) is compulsory
        # Line Layer (Road) is optional
        if(self.dlg.combo_building.count() == 0 and
           self.dlg.combo_road.count() == 0):
            # Case 1: When no layer is loaded
            self.dlg.status_label.setText("No Input Layer Detected! Please "
                                          "Load Shapefiles.")
            self.dlg.widget_box1.setEnabled(False)
        elif(self.dlg.combo_building.count() == 0):
            # Case 2: When no polygon(building) layer is present
            self.dlg.status_label.setText("Polygon Layer(Buildings) "
                                          "is compulsory.")
            self.dlg.widget_box1.setEnabled(False)
        else:
            # Case 3: When atleast polygon layer is present
            self.dlg.status_label.setText("Choose the required layers.")
            self.dlg.widget_box1.setEnabled(True)

    def disableTabs(self):
        """Disable all the other tabs apart from the building tab
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        for i in range(5):
            if(i == 0):
                self.dlg.tabWidget.setTabEnabled(i, True)
            else:
                self.dlg.tabWidget.setTabEnabled(i, False)

    def clearLayout(self, layout):
        """Clears the extended options sroll area.
        :param layout: The layout of the second Scroll Area
        :type layout: QLayout

        :returns: None
        :rtype: None
        """
        for j in range(layout.count()):
            # Getting reference to the chechbox widget
            item = layout.takeAt(0).widget()
            # Setting the parent of the checkbox to Nonr
            item.setParent(None)
            # Removing the checkbox
            layout.removeWidget(item)

    def getTechFileData(self):
        """Reads the data from the technologies metdata JSON file
           and creates list out of them.
        :param path: The path of technologies.csv file
        :type path: String

        :returns list_1: The list of technologies in Scroll Area 1
        :rtype: list

        :returns list_2: The list of technologies in Scroll Area 2
        :rtype: list
        """
        with open(self.tech_param_file_path) as data_file:
            data = json.load(data_file)
        t1 = []
        t2 = []
        for i in range(len(data)):
            for j in range(len(data[data.keys()[i]])):
                if(j == 0):
                    t1.append(
                        data[data.keys()[i]][data[data.keys()[i]].keys()[j]])
                else:
                    t2.append(
                        data[data.keys()[i]][data[data.keys()[i]].keys()[j]])
        return data.keys(), t1, t2

    def updateScrollsTab2(self):
        """update the contents of the ScrollAreas of TAB2
           based on the item selected in the listwidget of TAB2
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # TODO: This code can be modularized
        # Getting layout and clearing the contents of ScrollArea1
        temp_layout = self.dlg.techParamListContents.layout()
        self.clearLayout(temp_layout)
        temp_layout.update()

        # Adding new items to the scrollArea2
        row = self.dlg.technologiesList.currentRow()
        for i in range(len(self.lineEdit_list_param_tab2[row])):
            temp_layout.addWidget(self.lineEdit_list_param_tab2[row][i])

        # Getting layout and clearing the contents of ScrollArea2
        temp_layout = self.dlg.outputsParamListContents.layout()
        self.clearLayout(temp_layout)
        temp_layout.update()

        # Adding new items to the scrollArea1
        row = self.dlg.technologiesList.currentRow()
        for i in range(len(self.lineEdit_list_output_tab2[row])):
            temp_layout.addWidget(self.lineEdit_list_output_tab2[row][i])

        # # Getting layout and clearing the contents of ScrollArea3
        # temp_layout = self.dlg.carbonFactorListContents.layout()
        # self.clearLayout(temp_layout)
        # temp_layout.update()
        #
        # # Adding new items to the scrollArea3
        # row = self.dlg.technologiesList.currentRow()
        # for i in range(len(self.lineEdit_list_carbon_tab2[row])):
        #     temp_layout.addWidget(self.lineEdit_list_carbon_tab2[row][i])

    def initializeTechnologies(self):
        """Populating the ScrollAreas in Tab2
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Path of the JSON file
        self.tech_param_file_path = os.path.join(self.plugin_dir,
                                                 self.filename_technologies)

        # Get the list of technologies and their sub-options
        self.scroll_1_list_tech_tab2, self.scroll_4_list_carbon_tab2, \
            self.scroll_3_list_param_tab2 = self.getTechFileData()
        self.scroll_2_list_output_tab2 = \
            [self.list_output]*len(self.scroll_1_list_tech_tab2)

        # Adding the items to the list widget
        self.dlg.technologiesList.clear()
        self.dlg.technologiesList.addItems(self.scroll_1_list_tech_tab2)

        # Getting the LineEdits for all the three ScrollAreas i.e:
        # techParamList, outputsParamList, carbonFactorsList
        self.lineEdit_list_output_tab2 = self.getLineEditList(
            self.scroll_2_list_output_tab2)
        self.scroll_2_layout_tab2 = QVBoxLayout()
        self.dlg.techParamListContents.setLayout(self.scroll_2_layout_tab2)

        self.lineEdit_list_param_tab2 = self.getLineEditList(
            self.scroll_3_list_param_tab2)
        self.scroll_3_layout_tab2 = QVBoxLayout()
        self.dlg.outputsParamListContents.setLayout(self.scroll_3_layout_tab2)

        # self.lineEdit_list_carbon_tab2 = self.getLineEditList(
        #     self.scroll_4_list_carbon_tab2)
        # self.scroll_4_layout_tab2 = QVBoxLayout()
        # self.dlg.carbonFactorListContents.setLayout(self.scroll_4_layout_tab2)

    def proceedTab1(self):
        """Connects the Proceed PushButton from Tab1, to follow to the next tab
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Issue Warnings and give user one more chance
        val = self.issueWarning(1, self.tab1_flag)
        if(val == 0 and self.tab1_flag == 0):
            self.tab1_flag = 1
            return
        elif(val == 0 and self.tab1_flag == 1):
            self.tab1_flag = 0
        else:
            self.tab1_flag = 0

        # Finding and visualizing the centroids
        # of the polygons in the buildings layer
        layer = QgsMapLayerRegistry.instance().mapLayersByName(
            self.dlg.combo_building.currentLayer().name())[0]
        epsg = layer.crs().postgisSrid()
        uri = "Point?crs=epsg:" + str(epsg) + "&field=id:integer""&index=yes"
        self.mem_point_layer = QgsVectorLayer(uri, 'buildings_point', 'memory')
        prov = self.mem_point_layer.dataProvider()
        i = 0
        for f in layer.getFeatures():
            feat = QgsFeature()
            pt = f.geometry().centroid().asPoint()
            feat.setAttributes([i])
            feat.setGeometry(QgsGeometry.fromPoint(pt))
            prov.addFeatures([feat])
            i += 1
        QgsMapLayerRegistry.instance().addMapLayer(self.mem_point_layer, True)
        self.iface.mapCanvas().refresh()

        # Creating Distance Matrix
        num_vertices = self.mem_point_layer.featureCount()
        self.temp_mat = []
        for i in range(num_vertices):
            temp_arr = []
            for j in range(num_vertices):
                temp_arr.append(0)
            self.temp_mat.append(temp_arr)

        # Reading the Demand Data from excel field
        if(self.dlg.openPath.text() != ''):
            self.demand_data_file = ExcelFile(self.dlg.openPath.text())
            sheet_name = 'demand data'
            sheet_names = [
                str(x.lower()) for x in self.demand_data_file.sheet_names]
            if(sheet_name in sheet_names):
                self.demand_data = self.demand_data_file.parse(
                    sheet_names.index(sheet_name))
        else:
            self.demand_data = None

        # Move to the second tab for defining the technologies.
        # Diable the Tab1 and enable Tab2 (you cannot navigate back)
        self.dlg.tabWidget.setCurrentIndex(1)
        self.dlg.tabWidget.setTabEnabled(1, True)
        self.dlg.tabWidget.setTabEnabled(0, False)

        # Updating the scrollAreas
        self.initializeTechnologies()

    def proceedTab2(self):
        """Connects the Proceed PushButton from Tab2, to follow to the next tab
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Issue Warnings and give user one more chance
        val = self.issueWarning(2, self.tab2_flag)
        if(val == 0 and self.tab2_flag == 0):
            self.tab2_flag = 1
            return
        elif(val == 0 and self.tab2_flag == 1):
            self.tab2_flag = 0
        else:
            self.tab2_flag = 0

        # Diable the Tab2 and enable Tab3 (you cannot navigate back)
        self.dlg.tabWidget.setCurrentIndex(2)
        self.dlg.tabWidget.setTabEnabled(2, True)
        self.dlg.tabWidget.setTabEnabled(1, False)

        # The status box is read-only
        self.dlg.statusBoxTab3.setReadOnly(True)

        # Adding elements to the ListWidget of TAB3
        self.dlg.outputsListTab3.addItems(self.list_output)

        # Setting Layout to the ScrollArea for TAB3
        self.scroll_1_layout_tab3 = QVBoxLayout()
        self.dlg.storageListContentsTab3.setLayout(self.scroll_1_layout_tab3)

    def proceedTab3(self):
        """Connects the Proceed PushButton from Tab3, to follow to the next tab
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Issue Warnings and give user one more chance
        val = self.issueWarning(3, self.tab3_flag)
        if(val == 0 and self.tab3_flag == 0):
            self.tab3_flag = 1
            return
        elif(val == 0 and self.tab3_flag == 1):
            self.tab3_flag = 0
        else:
            self.tab3_flag = 0

        # Making the Status Bar Read-Only
        self.dlg.statusBarTab4.setReadOnly(True)

        if(self.dlg.localChangesCheckBox.isChecked()):
            # Open the dock to make changes locally
            # adding the GUI for the dockwidget
            self.iface.addDockWidget(Qt.RightDockWidgetArea, self.dock_tab3_1)

            # Disabling the plugin dialog box
            self.dlg.setDisabled(True)

            # populating the comboboxes of the dock widget
            self.dock_tab3_1.techCombo.addItems(
                [""] + self.scroll_1_list_tech_tab2)
            self.dock_tab3_1.storageCombo.addItems([""] + self.list_output)

            # Creating the dictionary to store all the tech data for the hub
            temp_dict_1 = {}
            for i in range(len(self.scroll_1_list_tech_tab2)):
                temp_dict_2 = {}
                temp_dict_2[u"Enabled"] = 1
                for j in range(len(self.lineEdit_list_param_tab2[i])):
                    temp_dict_2[
                            self.lineEdit_list_param_tab2[i][
                                j].getLabel()] = self.lineEdit_list_param_tab2[
                                    i][j].getValue()
                temp_dict_1[self.scroll_1_list_tech_tab2[i]] = temp_dict_2

            # Creating the dictionary to store the storage options data for hub
            temp_dict_3 = {}
            for i in range(len(self.list_output)):
                temp_dict_4 = {}
                temp_dict_4[u"Enabled"] = 1
                for j in range(len(self.list_storage_param[i])):
                    temp_dict_4[
                            self.list_storage_param[i][
                                j].getLabel()] = self.list_storage_param[
                                    i][j].getValue()
                temp_dict_3[self.list_output[i]] = temp_dict_4

            temp_dict_5 = {}
            temp_dict_5[u"Technologies"] = temp_dict_1
            temp_dict_5[u"Storages"] = temp_dict_3

            # TODO: After making this dictionary we can
            # free/delete all the previously created lists
            # The final dictionary which contains all the data
            self.data_dictionary = {}
            self.test_dd = {}
            feat_itr = self.mem_point_layer.getFeatures()
            for feat in feat_itr:
                self.data_dictionary[feat.attribute('id')] = copy.deepcopy(
                    temp_dict_5)
                self.test_dd[feat.attribute('id')] = 1

            # Freeing up the memory
            del temp_dict_2
            del temp_dict_1

            # Setting the layouts for the scrollAreas of the dockwidget
            self.scroll_1_layout_dock_tab3 = QVBoxLayout()
            self.dock_tab3_1.scrollArea1_1DockContents.setLayout(
                self.scroll_1_layout_dock_tab3)

            self.scroll_2_layout_dock_tab3 = QVBoxLayout()
            self.dock_tab3_1.scrollArea2_1DockContents.setLayout(
                self.scroll_2_layout_dock_tab3)

            # Filling up the Combo Box
            self.dlg.comboBoxDirectMethod.addItems(['Delaunay Triangulation',
                                                    'Minimum Spanning Tree'])

        else:
            # Diable the Tab3 and enable Tab4 (you cannot navigate back)
            self.dlg.tabWidget.setCurrentIndex(3)
            self.dlg.tabWidget.setTabEnabled(3, True)
            self.dlg.tabWidget.setTabEnabled(2, False)

            # Disabling the second Radio Button if no road layer was loaded
            if(self.dlg.combo_road.currentText() == ''):
                self.dlg.radioTab3Opt2.setDisabled(True)
            self.dlg.radioTab3Opt1.setChecked(True)

            # Disabling the radius checkbox and corresponding frame
            self.dlg.point_radius_check.setChecked(False)
            self.dlg.point_radius_frame.setDisabled(True)

            # Setting the status box to read-only
            self.dlg.point_radius_frame_status.setReadOnly(True)

            # Initializing the spinbox
            self.dlg.radiusSpin.setRange(100, 10000)
            self.dlg.radiusSpin.setSingleStep(1)

            # Filling up the Combo Box
            self.dlg.comboBoxDirectMethod.addItems(['Delaunay Triangulation',
                                                    'Minimum Spanning Tree'])

    def proceedTab4(self):
        """Connects the Confirm PushButton from Tab4, to follow to the next tab
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Diable the Tab4 and enable Tab5 (you cannot navigate back)
        self.dlg.tabWidget.setCurrentIndex(4)
        self.dlg.tabWidget.setTabEnabled(4, True)
        self.dlg.tabWidget.setTabEnabled(3, False)

        # Disabling the Save Button Initially
        self.dlg.saveButton.setDisabled(True)

        # Setting the LineEdit for path to be read-only
        self.dlg.savePath.setReadOnly(True)

        # Initializing the check boxes of TAB 5
        self.dlg.csvCheck.setChecked(True)
        self.dlg.excelCheck.setChecked(True)

    def proceedDockTab4(self):
        """Closes the dock and changes the TAB
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Closing the dock
        self.dock_tab4_1.close()

        # Disabling the plugin dialog box
        self.dlg.setDisabled(False)

        # Eanbling the dlg back
        self.dlg.setEnabled(True)

        # Diable the Tab3 and enable Tab4 (you cannot navigate back)
        self.proceedTab4()

    def radioControl(self, radio1, radio2, frame1, frame2):
        """Controls the radio button with the corresponding frames
        :param radio1: the first radio button
        :type radio1: QRadioButton

        :param radio2: the second radio button
        :type radio2: QRadioButton

        :param frame1: the first frame
        :type frame1: QFrame

        :param frame2: the second frame
        :type frame2: QFrame

        :returns: None
        :rtype: None
        """
        if(radio1.isChecked()):
            frame1.setEnabled(True)
            frame2.setEnabled(False)
        else:
            frame1.setEnabled(False)
            frame2.setEnabled(True)

    def dockOkButtonControl(self, ok_id, combo1, combo2, status):
        """Controls the OK buttons on the dock widget
        :param ok_id: Id to determine whic of the three Ok buttons was pressed
        :type ok_id: Integer

        :param combo1: the first combo box
        :type combo1: QComboBox

        :param combo2: the second combo box
        :type combo2: QComboBox

        :param status: the box to display the status of options
        :type status: QTextEdit

        :returns: None
        :rtype: None
        """
        if(ok_id == 1):
            # Case 1: Adding line between two points
            # getting the two points with geometry
            pt1_val = int(combo1.currentText())
            pt2_val = int(combo2.currentText())

            # If both the values are same then show status and ignore
            if(pt1_val == pt2_val):
                self.dock_tab4_1.statusBox.setText(
                    "The Source and the Destination points must be different!")
                return

            # If the line already exists
            if(pt1_val < pt2_val):
                temp_pair = (pt1_val, pt2_val)
            else:
                temp_pair = (pt2_val, pt1_val)
            if(temp_pair in self.line_feat_list_points):
                self.dock_tab4_1.statusBox.setText(
                    "The Line Between points {} and {} already exists!".format(
                        str(pt1_val), str(pt2_val)))
                return

            feat_itr = self.mem_point_layer.getFeatures()
            flag = 0
            for feat in feat_itr:
                if(flag == 2):
                    break
                if(feat.attribute('id') == pt1_val):
                    pt1 = feat.geometry().asPoint()
                    flag = flag + 1
                elif(feat.attribute('id') == pt2_val):
                    pt2 = feat.geometry().asPoint()
                    flag = flag + 1

            # Making the new Line
            Line = QgsFeature()
            Line.setGeometry(QgsGeometry.fromPolyline([pt1, pt2]))
            Line_geom = Line.geometry().asPolyline()
            temp_numbers = [int(x) for x in self.line_feat_list]
            max_val = max(temp_numbers)
            Line.setAttributes([int(max_val + 1),
                                self.getDistance(Line_geom[0], Line_geom[-1]),
                                pt1_val, pt2_val])
            self.line_feat_list.append(str(max_val + 1))
            self.line_feat_list_points.append((int(pt1_val), int(pt2_val)))

            # Adding the line, commit the changes and refresh the layers
            self.mem_line_veclayer.startEditing()
            # Checking whether the line was successfully added or not
            print(self.mem_line_veclayer.dataProvider().addFeatures([Line]))
            self.mem_line_veclayer.commitChanges()
            self.mem_line_veclayer.updateExtents()
            self.iface.mapCanvas().refresh()

            # Showing the status
            self.dock_tab4_1.statusBox.setText(
                "New Line Added between points {} and {}!".format(
                    str(pt1_val), str(pt2_val)))

        elif(ok_id == 2):
            # Case 2: Removing line between two points
            # getting the two points with geometry
            pt1_val = int(combo1.currentText())
            pt2_val = int(combo2.currentText())

            # If both the values are same then show status and ignore
            if(pt1_val == pt2_val):
                self.dock_tab4_1.statusBox.setText(
                    "The Source and the Destination points must be different!")
                return

            # If the line does not exist
            if(pt1_val < pt2_val):
                temp_pair = (pt1_val, pt2_val)
            else:
                temp_pair = (pt2_val, pt1_val)

            if(temp_pair not in self.line_feat_list_points):
                self.dock_tab4_1.statusBox.setText(
                    "No Line exists between points {} and {}!".format(
                        str(pt1_val), str(pt2_val)))
                return
            else:
                # Finding the feature to be deleted
                ind = self.line_feat_list_points.index(temp_pair)
                temp_cat = self.line_feat_list[ind]
                feat_itr = self.mem_line_veclayer.getFeatures()
                temp_feat = None
                for feat in feat_itr:
                    if(int(feat.attribute('cat')) == int(temp_cat)):
                        temp_feat = feat
                        break

                # Removing the line, commit the changes and refresh the layers
                self.mem_line_veclayer.startEditing()
                # Checking whether the line was successfully added or not
                print(self.mem_line_veclayer.dataProvider().deleteFeatures(
                    [temp_feat.id()]))
                self.mem_line_veclayer.commitChanges()
                self.mem_line_veclayer.updateExtents()
                self.iface.mapCanvas().refresh()

                # Showing the status
                self.dock_tab4_1.statusBox.setText(
                    "Removed the Line between points {} and {}!".format(
                        str(pt1_val), str(pt2_val)))

                # Removing from the lists
                self.line_feat_list_points.pop(ind)
                self.line_feat_list.pop(ind)

        else:
            # Case 2: Remvoing line from number
            # getting the line with geometry
            line_val = combo1.currentText()
            ind = self.line_feat_list.index(line_val)
            feat_itr = self.mem_line_veclayer.getFeatures()
            temp_feat = None
            for feat in feat_itr:
                if(int(feat.attribute('cat')) == int(line_val)):
                    temp_feat = feat
                    break

            # Removing the line, commit the changes and refresh the layers
            self.mem_line_veclayer.startEditing()
            # Checking whether the line was successfully added or not
            print(self.mem_line_veclayer.dataProvider().deleteFeatures(
                [temp_feat.id()]))
            self.mem_line_veclayer.commitChanges()
            self.mem_line_veclayer.updateExtents()
            self.iface.mapCanvas().refresh()

            # Showing the status
            self.dock_tab4_1.statusBox.setText(
                "Removed the Line with ID: {}!".format(line_val))

            # Removing from the lists
            self.line_feat_list_points.pop(ind)
            self.line_feat_list.pop(ind)

    def updateStorageListTab3(self):
        """Updates the storage list scroll area of TAB3
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Getting layout and clearing the contents
        temp_layout = self.dlg.storageListContentsTab3.layout()
        self.clearLayout(temp_layout)
        temp_layout.update()

        # Adding new items to the scrollArea
        row = self.dlg.outputsListTab3.currentRow()
        for i in range(len(self.list_storage_param[row])):
            temp_layout.addWidget(self.list_storage_param[row][i])

    def buttonControl(self, button, lineEdit=None, listwidget=None):
        """Disabling/Enabling the button based on
        the status of the passed paramters.
        :param button: button which needs to be enbled or disabled
        :type button: QButton

        :param lineEdit: listEdit on which the control depends
        :type lineEdit: QLineEdit

        :param listwidget: listWidget on which the control depends
        :type listwidget: QListWidget

        :returns: None
        :rtype: None
        """
        if(listwidget is not None):
            if(lineEdit.text() == '' or listwidget.count() == 0):
                button.setDisabled(True)
            else:
                button.setDisabled(False)
        else:
            if(lineEdit.text() == ''):
                button.setDisabled(True)
            else:
                button.setDisabled(False)

    def getStorageOptionsList(self):
        """Getting the uniques list of all Storage options avaliable
        :param None:
        :type None:

        :returns: A list of unique storage options
        :rtype: list
        """
        set_temp = set()
        for i in range(len(self.list_output_param)):
            set_temp |= set(self.list_output_param[i])
        return list(set_temp)

    def getTechnologyValues(self):
        """Getting the uniques list of all Technology
        Parameters and the Carbon Factors
        :param None:
        :type None:

        :returns: A list of unique Parameters for a Technology
        :rtype: list

        :returns: A list of unique Carbon Factors for a Technology
        :rtype: list
        """
        set_temp1 = set()
        set_temp2 = set()
        for i in range(len(self.scroll_3_list_param_tab2)):
            set_temp1 |= set(self.scroll_3_list_param_tab2[i])
            set_temp2 |= set(self.scroll_4_list_carbon_tab2[i])
        return list(set_temp1), list(set_temp2)

    def nameAddEnableAddTab2(self):
        """Used to Enable the TextEdits if the name
        value field is filled partially in Tab2 Add Dialog
        :param None:
        :type None:

        :returns: None
        :rtype: Nones
        """
        if(self.add_dlg_tab2.name.text() == ''):
            self.add_dlg_tab2.addNewEdit1.setDisabled(True)
            # self.add_dlg_tab2.addNewEdit2.setDisabled(True)
            self.add_dlg_tab2.okButton.setDisabled(True)
        else:
            self.add_dlg_tab2.addNewEdit1.setDisabled(False)
            # self.add_dlg_tab2.addNewEdit2.setDisabled(False)
            self.add_dlg_tab2.okButton.setDisabled(False)

    def moveButtonAddDialogTab3(self, opt, list_1, list_2):
        """Move the paramters from the left list to the
        right list and vice versa
        :param opt: Flag to determine if it is the left move
                    button or the right move button
        :type opt: Integer

        :param list_1: The listwidget on the left hand side
        :type list_1: QListWidget

        :param list_2: The listwidget on the right hand side
        :type list_2: QListWidget

        :returns: None
        :rtype: None
        """
        if(opt):
            # Moving from right list to the left list
            if(list_2.currentRow() == -1):
                return
            else:
                list_1.addItem(str(list_2.item(list_2.currentRow()).text()))
                list_2.takeItem(list_2.currentRow())
                # self.buttonControl(self.add_dlg_tab1.okButton,
                # self.add_dlg_tab1.name,
                # self.add_dlg_tab1.list_1)
        else:
            # Moving from left list to the right list
            if(list_1.currentRow() == -1):
                return
            else:
                list_2.addItem(str(list_1.item(list_1.currentRow()).text()))
                list_1.takeItem(list_1.currentRow())
                # self.buttonControl(self.add_dlg_tab1.okButton,
                # self.add_dlg_tab1.name,
                # self.add_dlg_tab1.list_1

    def moveButtonEditDialogTab2(self, opt, list_1, list_2):
        """Add new Technology Output to the list
        :param opt: Flag to determine if it is the left move
                    button or the right move button
        :type opt: Integer

        :param list_1: The listwidget on the left hand side
        :type list_1: QListWidget

        :param list_2: The listwidget on the right hand side
        :type list_2: QListWidget

        :returns: None
        :rtype: None
        """
        if(opt):
            # Moving from right list to the left list
            if(list_2.currentRow() == -1):
                return
            else:
                list_1.addItem(str(list_2.item(list_2.currentRow()).text()))
                list_2.takeItem(list_2.currentRow())
        else:
            # Moving from left list to the right list
            if(list_1.currentRow() == -1):
                return
            else:
                list_2.addItem(str(list_1.item(list_1.currentRow()).text()))
                list_1.takeItem(list_1.currentRow())

    def moveButtonAddDialogTab2(self, opt, list_1, list_2):
        """Add new Technology Output to the list
        :param opt: Flag to determine if it is the left move
                    button or the right move button
        :type opt: Integer

        :param list_1: The listwidget on the left hand side
        :type list_1: QListWidget

        :param list_2: The listwidget on the right hand side
        :type list_2: QListWidget

        :returns: None
        :rtype: None
        """
        if(opt):
            # Moving from right list to the left list
            if(list_2.currentRow() == -1):
                return
            else:
                list_1.addItem(str(list_2.item(list_2.currentRow()).text()))
                list_2.takeItem(list_2.currentRow())
        else:
            # Moving from left list to the right list
            if(list_1.currentRow() == -1):
                return
            else:
                list_2.addItem(str(list_1.item(list_1.currentRow()).text()))
                list_1.takeItem(list_1.currentRow())

    def addNewStorageOptionAddTab1(self):
        """Add new Storage Option to the list on the left in the add Dialog
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        self.add_dlg_tab1.list_1.addItem(self.add_dlg_tab1.addNewEdit.text())
        self.buttonControl(self.add_dlg_tab1.okButton,
                           self.add_dlg_tab1.name,
                           self.add_dlg_tab1.list_1)
        self.add_dlg_tab1.addNewEdit.setText("")
        self.add_dlg_tab1.addNewButton.setDisabled(True)

    def addNewStorageOptionEditTab3(self):
        """Add new Storage Option to the list on the left in the Edit Dialog
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        self.edit_dlg_tab3.list_1.addItem(self.edit_dlg_tab3.addNewEdit.text())
        self.edit_dlg_tab3.addNewEdit.setText("")
        self.edit_dlg_tab3.addNewButton.setDisabled(True)

    def addNewTechParamEditTab2(self):
        """Add new Technology Paramters to the
        list on the left in the Edit Dialog
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        self.edit_dlg_tab2.list_11.addItem(
            self.edit_dlg_tab2.addNewEdit1.text())
        self.edit_dlg_tab2.addNewEdit1.setText("")
        self.edit_dlg_tab2.addNewButton1.setDisabled(True)

    def addNewTechCarbonEditTab2(self):
        """Add new Technology Carbon Factor Option
        to the list on the left in the Edit Dialog
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        self.edit_dlg_tab2.list_12.addItem(
            self.edit_dlg_tab2.addNewEdit2.text())
        self.edit_dlg_tab2.addNewEdit2.setText("")
        self.edit_dlg_tab2.addNewButton2.setDisabled(True)

    def addNewTechParamAddTab2(self):
        """Add new Technology Paramters to the
        list on the left in the Add Dialog
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        self.add_dlg_tab2.list_11.addItem(
            self.add_dlg_tab2.addNewEdit1.text())
        self.add_dlg_tab2.addNewEdit1.setText("")
        self.add_dlg_tab2.addNewButton1.setDisabled(True)

    def addNewTechCarbonAddTab2(self):
        """Add new Technology Carbon Factor Option
        to the list on the left in the Add Dialog
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        self.add_dlg_tab2.list_12.addItem(
            self.add_dlg_tab2.addNewEdit2.text())
        self.add_dlg_tab2.addNewEdit2.setText("")
        self.add_dlg_tab2.addNewButton2.setDisabled(True)

    def updateListsEditTab3(self):
        """Updates the ListWidgets of the Edit Dialog of the TAB3
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Clearing the lists
        self.edit_dlg_tab3.list_1.clear()
        self.edit_dlg_tab3.list_2.clear()

        # Getting the complete storage options
        list_full = self.getStorageOptionsList()

        # Checking if the current text is blank or not
        curtxt = self.edit_dlg_tab3.outputCombo.currentText()
        if(curtxt == ''):
            self.edit_dlg_tab3.list_2.clear()
            self.edit_dlg_tab3.list_1.clear()
            self.edit_dlg_tab3.addNewEdit.setDisabled(True)
            self.edit_dlg_tab3.statusBoxTab3.setText(
                "Select Technology Output from the List")
            return

        # Setting the new text on the edit dialog
        self.edit_dlg_tab3.statusBoxTab3.setText(
            "Editing changes to the " + str(
                self.edit_dlg_tab3.outputCombo.currentText()) +
            "\nPrevious changes are lost!!")

        # Enabling the person to enter value to the text edit
        # only if a value has been selected in the combo box
        self.edit_dlg_tab3.addNewEdit.setDisabled(False)

        # Getting the index
        index = self.list_output.index(curtxt)

        # Getting the left list storage options
        list_left = []
        for i in range(len(self.list_output_param[index])):
            list_left.append(self.list_output_param[index][i])

        # Getting the right list storage options
        list_right = list(set(list_full) - set(list_left))

        # Updateing the lists
        self.edit_dlg_tab3.list_2.addItems(list_right)
        self.edit_dlg_tab3.list_1.addItems(list_left)

    def updateListsEditTab2(self):
        """Updates the ListWidgets of the Edit Dialog of the TAB2
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Clearing the lists
        self.edit_dlg_tab2.list_11.clear()
        self.edit_dlg_tab2.list_21.clear()
        # self.edit_dlg_tab2.list_12.clear()
        # self.edit_dlg_tab2.list_22.clear()

        # Getting the complete Paramters and Carbon Factors List
        list_full_param, list_full_carbon = self.getTechnologyValues()
        # TODO: Modularization on getStorageOptionsList and getTechnologyValues

        # Checking if the current text is blank or not
        curtxt = self.edit_dlg_tab2.outputCombo.currentText()
        if(curtxt == ''):
            self.edit_dlg_tab2.list_11.clear()
            self.edit_dlg_tab2.list_21.clear()
            # self.edit_dlg_tab2.list_12.clear()
            # self.edit_dlg_tab2.list_22.clear()
            self.edit_dlg_tab2.addNewEdit1.setDisabled(True)
            # self.edit_dlg_tab2.addNewEdit2.setDisabled(True)
            self.edit_dlg_tab2.statusBoxTab2.setText(
                "Select Technology Output from the List")
            return

        # Setting the new text on the edit dialog
        self.edit_dlg_tab2.statusBoxTab2.setText(
            "Editing changes to the " + str(
                self.edit_dlg_tab2.outputCombo.currentText()) +
            "\nPrevious changes are lost!!")

        # Enabling the person to enter value to the text edit
        # only if a value has been selected in the combo box
        self.edit_dlg_tab2.addNewEdit1.setDisabled(False)
        # self.edit_dlg_tab2.addNewEdit2.setDisabled(False)

        # Getting the index
        index = self.scroll_1_list_tech_tab2.index(curtxt)

        # Getting the left list parameters and carbon factors
        list_left_param = []
        # list_left_carbon = []
        for i in range(len(self.scroll_3_list_param_tab2[index])):
            list_left_param.append(self.scroll_3_list_param_tab2[index][i])

        # for i in range(len(self.scroll_4_list_carbon_tab2[index])):
        #     list_left_carbon.append(self.scroll_4_list_carbon_tab2[index][i])

        # Getting the right list storage options
        list_right_param = list(set(list_full_param) - set(list_left_param))
        # list_right_carbon = list(set(list_full_carbon)-set(list_left_carbon))

        # Updateing the lists
        self.edit_dlg_tab2.list_21.addItems(list_right_param)
        self.edit_dlg_tab2.list_11.addItems(list_left_param)
        # self.edit_dlg_tab2.list_22.addItems(list_right_carbon)
        # self.edit_dlg_tab2.list_12.addItems(list_left_carbon)

    def editOutputTab3(self):
        """Edit the Technology Output or Storage Option
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Basic initialization
        self.edit_dlg_tab3.show()
        self.dlg.setDisabled(True)
        self.edit_dlg_tab3.addNewEdit.setDisabled(True)
        self.edit_dlg_tab3.addNewButton.setDisabled(True)

        # Initial status when the plugin is just started
        self.edit_dlg_tab3.statusBoxTab3.setReadOnly(True)
        self.edit_dlg_tab3.statusBoxTab3.setText(
            "Select Technology Output from the List")

        # Clearing and populating the comboBox
        # and adding a blank item in the start
        self.edit_dlg_tab3.outputCombo.clear()
        self.edit_dlg_tab3.outputCombo.addItem("")
        self.edit_dlg_tab3.outputCombo.addItems(self.list_output)

        result = self.edit_dlg_tab3.exec_()
        self.dlg.setDisabled(False)

        if result:
            # Getting the index which has been modified
            index = self.list_output.index(
                self.edit_dlg_tab3.outputCombo.currentText())
            # Getting the new storge options
            temp1 = []
            temp2 = []
            for i in range(self.edit_dlg_tab3.list_1.count()):
                temp1.append(self.edit_dlg_tab3.list_1.item(i).text())
                temp_label_edit = LabelEdit(temp1[i]+str(":"), 2, 3)
                temp_label_edit.edit.textChanged.connect(partial(
                    self.tab1EditChanged))
                temp2.append(temp_label_edit)
            self.list_output_param[index] = temp1
            self.list_storage_param[index] = temp2

    def returnEditButtonTab3(self, val):
        """Closes the remove dialog box of TAB3
        based on which button is pressed
        :param val: Return code for the button
        :type val: Integer

        :returns: None
        :rtype: None
        """
        self.edit_dlg_tab3.done(val)

    def addOutputTab1(self):
        """Add new Technology Output to the list
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        self.add_dlg_tab1.show()
        self.dlg.setDisabled(True)

        # Clearing the lineEdit
        self.add_dlg_tab1.name.clear()

        # Disabling the button
        self.add_dlg_tab1.okButton.setDisabled(True)

        result = self.add_dlg_tab1.exec_()
        self.dlg.setDisabled(False)

        if result:
            # Adding new Technology to list
            self.list_output.append(self.add_dlg_tab1.name.text())
            # Adding blank list of storage options which will be modified later
            temp1 = []
            temp2 = []
            self.list_output_param.append(temp1)
            self.list_storage_param.append(temp2)
            # Refreshing the list widget on the TAB1
            self.dlg.outputsList.clear()
            self.dlg.outputsList.addItems(self.list_output)
            # Refreshing the ScrollArea on the TAB1
            temp_layout = self.dlg.exportPriceListContents.layout()
            self.clearLayout(temp_layout)
            temp_layout.update()
            temp1 = []
            for i in range(len(self.list_output)):
                temp_label_edit = LabelEdit(
                    self.list_output[i]+str(":"), 2, 3)
                temp1.append(temp_label_edit)
            for i in range(len(self.list_output)):
                temp_layout.addWidget(temp1[i])
            # Controlling the Porceed Button of the TAB1
            if(self.dlg.outputsList.count() == 0):
                self.dlg.proceedButton1.setDisabled(True)
            else:
                self.dlg.proceedButton1.setDisabled(False)

    def returnAddButtonTab1(self, val):
        """Closes the add dialog box of TAB1
        based on which button is pressed
        :param val: Return code for the button
        :type val: Integer

        :returns: None
        :rtype: None
        """
        self.add_dlg_tab1.done(val)

    def removeOutputTab1(self):
        """Remove a Technology Output from the list
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        self.remove_dlg_tab1.show()
        self.dlg.setDisabled(True)
        self.remove_dlg_tab1_okflag = 0

        # Clearing and populating the comboBox
        self.remove_dlg_tab1.outputCombo.clear()
        self.remove_dlg_tab1.outputCombo.addItems(self.list_output)
        self.remove_dlg_tab1.label.setText("Select a Technology Output")
        self.remove_dlg_tab1.label.setReadOnly(True)

        result = self.remove_dlg_tab1.exec_()
        self.dlg.setDisabled(False)

        if result:
            # Getting the index
            index = self.list_output.index(
                self.remove_dlg_tab1.outputCombo.currentText())
            # Refreshing the outputs list
            self.list_output.pop(index)
            self.list_output_param.pop(index)
            self.list_storage_param.pop(index)
            self.dlg.outputsList.clear()
            self.dlg.outputsList.addItems(self.list_output)
            # Refreshing the ScrollArea on the TAB1
            temp_layout = self.dlg.exportPriceListContents.layout()
            self.clearLayout(temp_layout)
            temp_layout.update()
            temp1 = []
            for i in range(len(self.list_output)):
                temp_label_edit = LabelEdit(
                    self.list_output[i]+str(":"), 2, 3)
                temp1.append(temp_label_edit)
            for i in range(len(self.list_output)):
                temp_layout.addWidget(temp1[i])
            # Controlling the Porceed Button of the TAB1
            if(self.dlg.outputsList.count() == 0):
                self.dlg.proceedButton1.setDisabled(True)
            else:
                self.dlg.proceedButton1.setDisabled(False)

    def returnRemoveButtonTab1(self, val):
        """Closes the remove dialog box of TAB1
        based on which button is pressed
        :param val: Return code for the button
        :type val: Integer

        :returns: None
        :rtype: None
        """
        if(val == 0):
            self.remove_dlg_tab1.done(val)
        else:
            if(self.remove_dlg_tab1_okflag == 0):
                self.remove_dlg_tab1.label.setText(
                    "Are you Sure you want to remove: \n" + str(
                        self.remove_dlg_tab1.outputCombo.currentText()) +
                    "\nPress Okay Again!"
                    )
                self.remove_dlg_tab1_okflag = 1
            else:
                self.remove_dlg_tab1.done(val)

    def editTechnologyTab2(self):
        """Edit the Technologies present in TAB2
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Basic initialization
        self.edit_dlg_tab2.show()
        self.dlg.setDisabled(True)
        self.edit_dlg_tab2.addNewEdit1.setDisabled(True)
        self.edit_dlg_tab2.addNewButton1.setDisabled(True)
        # self.edit_dlg_tab2.addNewEdit2.setDisabled(True)
        # self.edit_dlg_tab2.addNewButton2.setDisabled(True)

        # Initial status when the plugin is just started
        self.edit_dlg_tab2.statusBoxTab2.setReadOnly(True)
        self.edit_dlg_tab2.statusBoxTab2.setText(
            "Select Technology from the List")
        # TODO: Rename statusBoxTab2/statusBoxTab3 to statusBox

        # Clearing and populating the comboBox
        # and adding a blank item in the start
        self.edit_dlg_tab2.outputCombo.clear()
        self.edit_dlg_tab2.outputCombo.addItem("")
        self.edit_dlg_tab2.outputCombo.addItems(self.scroll_1_list_tech_tab2)

        # # Clearing both the lists and adding items to them
        # self.updateListsEditTab1()

        result = self.edit_dlg_tab2.exec_()
        self.dlg.setDisabled(False)

        if result:
            # Getting the index which has been modified
            index = self.scroll_1_list_tech_tab2.index(
                self.edit_dlg_tab2.outputCombo.currentText())
            # Getting the new parameters
            temp1 = []
            temp2 = []
            for i in range(self.edit_dlg_tab2.list_11.count()):
                temp1.append(self.edit_dlg_tab2.list_11.item(i).text())
                temp_label_edit = LabelEdit(str(temp1[i])+str(":"), 2, 3)
                temp_label_edit.edit.textChanged.connect(partial(
                    self.tab1EditChanged))
                temp2.append(temp_label_edit)
            self.scroll_3_list_param_tab2[index] = temp1
            self.lineEdit_list_param_tab2[index] = temp2
            # # Getting the new carbon factors
            # temp3 = []
            # temp4 = []
            # for i in range(self.edit_dlg_tab2.list_12.count()):
            #     temp3.append(self.edit_dlg_tab2.list_12.item(i).text())
            #     temp_label_edit = LabelEdit(str(temp3[i])+str(":"), 2, 3)
            #     temp_label_edit.edit.textChanged.connect(partial(
            #         self.tab1EditChanged))
            #     temp4.append(temp_label_edit)
            # self.scroll_4_list_carbon_tab2[index] = temp3
            # self.lineEdit_list_carbon_tab2[index] = temp4

    def returnEditButtonTab2(self, val):
        """Closes the remove dialog box of TAB2
        based on which button is pressed
        :param val: Return code for the button
        :type val: Integer

        :returns: None
        :rtype: None
        """
        self.edit_dlg_tab2.done(val)

    def addTechnologyTab2(self):
        """Add new Technology Output to the list
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Basic Initialization
        self.add_dlg_tab2.show()
        self.dlg.setDisabled(True)
        self.add_dlg_tab2.addNewEdit1.setDisabled(True)
        self.add_dlg_tab2.addNewButton1.setDisabled(True)
        # self.add_dlg_tab2.addNewEdit2.setDisabled(True)
        # self.add_dlg_tab2.addNewButton2.setDisabled(True)

        # Clearing the lineEdit
        self.add_dlg_tab2.name.clear()

        # Get and set the complete lists on the right side
        # Clearing the lists
        self.add_dlg_tab2.list_11.clear()
        self.add_dlg_tab2.list_21.clear()
        # self.add_dlg_tab2.list_12.clear()
        # self.add_dlg_tab2.list_22.clear()

        # Enabling dispatchable technology radio button
        self.add_dlg_tab2.dispTech.setChecked(True)
        self.add_dlg_tab2.solarTech.setChecked(False)

        # Getting the complete Paramters and Carbon Factors List
        list_full_param, list_full_carbon = self.getTechnologyValues()

        # Adding the items to the list
        self.add_dlg_tab2.list_21.addItems(list_full_param)
        # self.add_dlg_tab2.list_22.addItems(list_full_carbon)

        # Disabling the button
        self.add_dlg_tab2.okButton.setDisabled(True)

        result = self.add_dlg_tab2.exec_()
        self.dlg.setDisabled(False)

        if result:
            # Adding new Technology to list
            if(self.add_dlg_tab2.solarTech.isChecked()):
                self.scroll_1_list_tech_tab2.append(
                    self.add_dlg_tab2.name.text() + " (Solar)")
            else:
                self.scroll_1_list_tech_tab2.append(
                    self.add_dlg_tab2.name.text())
            # Adding technology parameters and carbon factors lists
            temp1 = []
            temp3 = []
            for i in range(self.add_dlg_tab2.list_11.count()):
                temp1.append(self.add_dlg_tab2.list_11.item(i).text())
                temp_label_edit = LabelEdit(temp1[i]+str(":"), 2, 3)
                temp_label_edit.edit.textChanged.connect(partial(
                    self.tab1EditChanged))
                temp3.append(temp_label_edit)
            # temp2 = []
            # temp4 = []
            # for i in range(self.add_dlg_tab2.list_12.count()):
            #     temp2.append(self.add_dlg_tab2.list_12.item(i).text())
            #     temp_label_edit = LabelEdit(temp2[i]+str(":"), 2, 3)
            #     temp_label_edit.edit.textChanged.connect(partial(
            #         self.tab1EditChanged))
            #     temp4.append(temp_label_edit)
            self.scroll_3_list_param_tab2.append(temp1)
            self.lineEdit_list_param_tab2.append(temp3)
            # self.scroll_4_list_carbon_tab2.append(temp2)
            # self.lineEdit_list_carbon_tab2.append(temp4)
            self.scroll_2_list_output_tab2.append(self.list_output)
            temp5 = []
            for i in range(len(self.list_output)):
                temp_label_edit = LabelEdit(self.list_output[i]+str(":"), 2, 3)
                temp_label_edit.edit.textChanged.connect(partial(
                    self.tab1EditChanged))
                temp5.append(temp_label_edit)
            self.lineEdit_list_output_tab2.append(temp5)
            # Adding technology parameters and carbon factors lineEdits
            # Refreshing the list widget on the TAB1
            self.dlg.technologiesList.clear()
            self.dlg.technologiesList.addItems(self.scroll_1_list_tech_tab2)

    def returnAddButtonTab2(self, val):
        """Closes the add dialog box of TAB2
        based on which button is pressed
        :param val: Return code for the button
        :type val: Integer

        :returns: None
        :rtype: None
        """
        self.add_dlg_tab2.done(val)

    def removeTechnologyTab2(self):
        """Remove a Technology Output from the list
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        self.remove_dlg_tab2.show()
        self.dlg.setDisabled(True)
        self.remove_dlg_tab2_okflag = 0

        # Clearing and populating the comboBox
        self.remove_dlg_tab2.outputCombo.clear()
        self.remove_dlg_tab2.outputCombo.addItems(self.scroll_1_list_tech_tab2)
        self.remove_dlg_tab2.label.setText("Select a Technology Output")
        self.remove_dlg_tab2.label.setReadOnly(True)

        result = self.remove_dlg_tab2.exec_()
        self.dlg.setDisabled(False)

        if result:
            # Getting the index
            index = self.scroll_1_list_tech_tab2.index(
                self.remove_dlg_tab2.outputCombo.currentText())

            # Refreshing all the lists and the scroll areas
            self.scroll_1_list_tech_tab2.pop(index)
            self.scroll_2_list_output_tab2.pop(index)
            self.lineEdit_list_output_tab2.pop(index)
            self.scroll_3_list_param_tab2.pop(index)
            self.lineEdit_list_param_tab2.pop(index)
            # self.scroll_4_list_carbon_tab2.pop(index)
            # self.lineEdit_list_carbon_tab2.pop(index)

            # Refreshing the technologies ListWidget
            self.dlg.technologiesList.clear()
            self.dlg.technologiesList.addItems(self.scroll_1_list_tech_tab2)

    def returnRemoveButtonTab2(self, val):
        """Closes the remove dialog box of TAB2
        based on which button is pressed
        :param val: Return code for the button
        :type val: Integer

        :returns: None
        :rtype: None
        """
        if(val == 0):
            self.remove_dlg_tab2.done(val)
        else:
            if(self.remove_dlg_tab2_okflag == 0):
                self.remove_dlg_tab2.label.setText(
                    "Are you Sure you want to remove: \n" + str(
                        self.remove_dlg_tab2.outputCombo.currentText()) +
                    "\nPress Okay Again!"
                    )
                self.remove_dlg_tab2_okflag = 1
            else:
                self.remove_dlg_tab2.done(val)

    def getDistance(self, point1, point2):
        """Calculates and return the distance for the points given
        :param point1: point 1 in form [x, y]
        :type point1: List

        :param point2: point 2 in form [x, y]
        :type point2: List

        :returns: the distance between the two points
        :rtype: Double
        """
        # Approximate radius of Earth
        R = 6373.0
        # getting the latitudes and longitudes of the points
        lat1 = radians(point1[0])
        lon1 = radians(point1[1])
        lat2 = radians(point2[0])
        lon2 = radians(point2[1])

        # Calculating the distance
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))
        return R * c

    def statusUpdateTab4(self):
        """Used to update the status bar on TAB4,
        Gives the user a warning that Steiner Tree is slow
        :param: None
        :type: None

        :returns: None
        :rtype: None
        """
        if(self.dlg.radioTab3Opt2.isChecked() or
           self.dlg.comboBoxDirectMethod.currentText(
           ) == 'Minimum Spanning Tree'):
            self.dlg.statusBarTab4.setText(
                "NOTE: Steiner is a Very Slow Running Algorithm!!" +
                " Make sure you have large RAM!!")
        else:
            self.dlg.statusBarTab4.setText("")

    def confirmTab4(self):
        """The action for the confirm button on the Tab4
        It creates the graph allowing for further operations
        :param: None
        :type: None

        :returns: None
        :rtype: None
        """
        if(self.dlg.radioTab3Opt1.isChecked()):
            # Case one: When direct connections are to be made
            # Creating the new line layer with direct connections
            # Getting the extent of the layer
            extent = self.mem_point_layer.extent()
            xmin = extent.xMinimum()
            xmax = extent.xMaximum()
            ymin = extent.yMinimum()
            ymax = extent.yMaximum()

            # The grass algorithm to create connections
            res = processing.runalg("grass7:v.to.lines",
                                    self.mem_point_layer,
                                    0,
                                    "%f,%f,%f,%f" % (xmin, xmax, ymin, ymax),
                                    -1,
                                    0.0001,
                                    2,
                                    None)

            # Getting the reference to the line layer
            self.mem_line_veclayer = QgsVectorLayer(
                res['output'], 'Energy Network', 'ogr')
            if(self.dlg.comboBoxDirectMethod.currentText(
            ) == 'Delaunay Triangulation'):
                QgsMapLayerRegistry.instance().addMapLayer(
                    self.mem_line_veclayer, True)
                self.iface.mapCanvas().refresh()

            # Removing Duplicate layers
            # layer_list = QgsMapLayerRegistry.instance().mapLayersByName(
            #     'Energy Network')
            # for i in range(len(layer_list) - 1):
            #     QgsMapLayerRegistry.instance().removeMapLayer(layer_list[i])
            # self.iface.mapCanvas().refresh()

            # Renaming the column because it creates a GRASS error
            self.mem_line_veclayer.startEditing()
            idx = self.mem_line_veclayer.fieldNameIndex('cat')
            self.mem_line_veclayer.renameAttribute(idx, 'cat2')
            self.mem_line_veclayer.commitChanges()
            self.mem_line_veclayer.updateExtents()
            self.iface.mapCanvas().refresh()

            # If the user selected Minimum Spanning Tree option
            if(self.dlg.comboBoxDirectMethod.currentText(
            ) == 'Minimum Spanning Tree'):
                # The grass algorithm to create the network
                try:
                    res = processing.runalg("grass7:v.net.connect",
                                            self.mem_line_veclayer,
                                            self.mem_point_layer,
                                            50.0,
                                            False,
                                            "%f,%f,%f,%f" % (xmin,
                                                             xmax, ymin, ymax),
                                            -1,
                                            0.0001,
                                            2,
                                            None)
                except Exception as e:
                    print(e)

                # Adding the network to the registry and canvas
                self.mem_direct_network_veclayer = QgsVectorLayer(
                    res['output'], 'network', 'ogr')

                # The grass algorithm to create the Minimum Spanning Tree
                try:
                    res = processing.runalg("grass7:v.net.steiner",
                                            self.mem_direct_network_veclayer,
                                            self.mem_point_layer,
                                            50.0,
                                            2,
                                            "1-10000",
                                            None,
                                            -1,
                                            False,
                                            "%f,%f,%f,%f" % (xmin,
                                                             xmax, ymin, ymax),
                                            -1,
                                            0.0001,
                                            2,
                                            None)
                except Exception as e:
                    print(e)

                # Adding the minimum spanning tree to the registry and canvas
                self.mem_line_veclayer = QgsVectorLayer(
                    res['output'], 'mst', 'ogr')
                QgsMapLayerRegistry.instance().addMapLayer(
                    self.mem_line_veclayer, True)
                self.iface.mapCanvas().refresh()

            # Creating data about length and end points
            prov = self.mem_line_veclayer.dataProvider()
            field1 = QgsField("length_km", QVariant.Double)
            field2 = QgsField("start_pt", QVariant.String)
            field3 = QgsField("end_pt", QVariant.String)
            prov.addAttributes([field1, field2, field3])
            self.mem_line_veclayer.updateFields()

            # Getting the feature Ids for the point layer
            temp_point_map = {}
            feat_itr = self.mem_point_layer.getFeatures()
            self.point_feat_list = []
            for feat in feat_itr:
                temp_point_map[QgsPoint(feat.geometry().asPoint())] = str(
                    feat.attribute('id'))
                self.point_feat_list.append(str(feat.attribute('id')))

            # Adding new fields in the attribute table: length, point1, point2
            # Also getting the feature Ids for the line layer
            self.line_feat_list = []
            # Storing the start nand end point reference
            self.line_feat_list_points = []
            feat_itr = self.mem_line_veclayer.getFeatures()
            for feat in feat_itr:
                # The list which is added in the combo box
                if(self.dlg.comboBoxDirectMethod.currentText(
                ) == 'Minimum Spanning Tree'):
                    self.line_feat_list.append(str(feat.attribute('cat')))
                else:
                    self.line_feat_list.append(str(feat.attribute('cat2')))
                line = feat.geometry().asPolyline()

                # Adding the calculated value in the attribute table
                prov.changeAttributeValues(
                    {feat.id(): {self.mem_line_veclayer.fieldNameIndex(
                        'length_km'): self.getDistance(line[0], line[-1])}})

                # Getting Start point
                start_point = QgsPoint(line[0])
                start_point_val = temp_point_map[start_point]
                prov.changeAttributeValues(
                    {feat.id(): {self.mem_line_veclayer.fieldNameIndex(
                        'start_pt'): start_point_val}})

                # Getting End point
                end_point = QgsPoint(line[-1])
                end_point_val = temp_point_map[end_point]
                prov.changeAttributeValues(
                    {feat.id(): {self.mem_line_veclayer.fieldNameIndex(
                        'end_pt'): end_point_val}})

                # Saving the values for future reference
                if(start_point_val < end_point_val):
                    self.line_feat_list_points.append(
                        (int(start_point_val), int(end_point_val)))
                else:
                    self.line_feat_list_points.append(
                        (int(end_point_val), int(start_point_val)))

                # Updating the change in the feature
                self.mem_line_veclayer.updateFeature(feat)
                self.mem_line_veclayer.commitChanges()
                self.mem_line_veclayer.updateExtents()
                self.iface.mapCanvas().refresh()

            # # Renaming the column because it creates a GRASS error
            if(self.dlg.comboBoxDirectMethod.currentText(
            ) == 'Delaunay Triangulation'):
                self.mem_line_veclayer.startEditing()
                idx = self.mem_line_veclayer.fieldNameIndex('cat2')
                self.mem_line_veclayer.renameAttribute(idx, 'cat')
                self.mem_line_veclayer.commitChanges()
                self.iface.mapCanvas().refresh()

            # adding the GUI for the dockwidget
            self.iface.addDockWidget(Qt.RightDockWidgetArea, self.dock_tab4_1)
            self.dock_tab4_1.statusBox.setReadOnly(True)

            # Enabling the radio button and corresponding frame
            self.dock_tab4_1.usePointRadio.setChecked(True)
            self.dock_tab4_1.frame_1.setEnabled(True)
            self.dock_tab4_1.useLineRadio.setChecked(False)
            self.dock_tab4_1.frame_2.setEnabled(False)

            # Populating the combo boxes of the dockwidget
            self.dock_tab4_1.addSrcPointCombo.addItems(self.point_feat_list)
            self.dock_tab4_1.addDstPointCombo.addItems(self.point_feat_list)
            self.dock_tab4_1.removeSrcPointCombo.addItems(self.point_feat_list)
            self.dock_tab4_1.removeDstPointCombo.addItems(self.point_feat_list)
            self.dock_tab4_1.lineCombo.addItems(self.line_feat_list)

            # Disabling the Dialog Box
            self.dlg.setEnabled(False)

            # Setting flag = 1 if we are making direct connections
            self.network_flag = 1

        else:
            # Case Two: When underlying road-graph layer has to be utilized
            # Getting the road layer index
            lyrs = self.iface.mapCanvas().layers()
            ly = []
            for lyr in lyrs:
                ly.append(lyr.name())
            ind = ly.index(self.dlg.combo_road.currentText())

            # Getting the extent of the road layer
            extent = lyrs[ind].extent()
            xmin = extent.xMinimum()
            xmax = extent.xMaximum()
            ymin = extent.yMinimum()
            ymax = extent.yMaximum()

            # The grass algorithm to clean the road layer
            try:
                res = processing.runalg("grass7:v.clean",
                                        lyrs[ind],
                                        0,
                                        0.1,
                                        "%f,%f,%f,%f" % (xmin,
                                                         xmax, ymin, ymax),
                                        -1,
                                        0.0001,
                                        None,
                                        None)
            except Exception as e:
                print(e)

            # Adding the cleaned road layer to the registry and canvas
            self.mem_cleaned_veclayer = QgsVectorLayer(
                res['output'], 'cleaned_road', 'ogr')
            # print(self.mem_cleaned_veclayer)
            # print(QgsMapLayerRegistry.instance().addMapLayer(
            #     self.mem_cleaned_veclayer, True))
            # self.iface.mapCanvas().refresh()

            # The grass algorithm to create the network
            try:
                res = processing.runalg("grass7:v.net.connect",
                                        self.mem_cleaned_veclayer,
                                        self.mem_point_layer,
                                        500.0,
                                        False,
                                        "%f,%f,%f,%f" % (xmin,
                                                         xmax, ymin, ymax),
                                        -1,
                                        0.0001,
                                        2,
                                        None)
            except Exception as e:
                print(e)

            # Adding the network to the registry and canvas
            self.mem_network_veclayer = QgsVectorLayer(
                res['output'], 'network', 'ogr')
            # print(self.mem_network_veclayer)
            # print(QgsMapLayerRegistry.instance().addMapLayer(
            #     self.mem_network_veclayer, True))
            # self.iface.mapCanvas().refresh()

            # The grass algorithm to create the steiner tree
            try:
                res = processing.runalg("grass7:v.net.steiner",
                                        self.mem_network_veclayer,
                                        self.mem_point_layer,
                                        50.0,
                                        2,
                                        "1-10000",
                                        None,
                                        -1,
                                        False,
                                        "%f,%f,%f,%f" % (xmin,
                                                         xmax, ymin, ymax),
                                        -1,
                                        0.0001,
                                        2,
                                        None)
            except Exception as e:
                print(e)

            # Adding the steiner tree to the registry and canvas
            self.mem_steiner_veclayer = QgsVectorLayer(
                res['output'], 'steiner', 'ogr')
            print(QgsMapLayerRegistry.instance().addMapLayer(
                self.mem_steiner_veclayer, True))
            self.iface.mapCanvas().refresh()

            # Setting flag = 2 if we are making road layer
            self.network_flag = 2

        # Disabling the point and radius option from using further
        self.dlg.point_radius_check.setDisabled(True)
        self.dlg.point_radius_frame.setDisabled(True)

        # only in case of the second option proceed to the next tab
        # The first option proceed will take place from the Connection Dock 1
        if(self.dlg.radioTab3Opt2.isChecked()):
            # Calculating the network distnaces
            self.getDistancesData()
            # Proceeding to the next tab
            self.proceedTab4()

    def pointRadiusCheck(self):
        """Based on the chekbox, enable or disable the frame
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        if(self.dlg.point_radius_check.isChecked()):
            self.dlg.point_radius_frame.setDisabled(False)
            self.dlg.point_radius_frame_status.setText("No point selected!!")
        else:
            self.dlg.point_radius_frame.setDisabled(True)
            self.dlg.point_radius_frame_status.setText("")

    def getPoints(self):
        """Get only those points which fall within the specified radius
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Getting the radius and converting to a double value in Km
        thresh = self.dlg.radiusSpin.value() / 1000.0

        # Setting up the input and output projections for conversion
        inProj = Proj(init='epsg:'+str(
            self.mem_point_layer.crs().postgisSrid()))
        outProj = Proj(init='epsg:4326')

        # Selecting features only within the given radius
        feat_itr = self.mem_point_layer.getFeatures()
        final_feat_list = []
        for feat in feat_itr:
            pt = feat.geometry().asPoint()
            x1, y1 = transform(inProj, outProj, self.mx, self.my)
            x2, y2 = transform(inProj, outProj, pt[0], pt[1])
            d = self.getDistance((x1, y1), (x2, y2))
            if(d > thresh):
                final_feat_list.append(feat.id())

        # Removing the points outside the radius
        # commit the changes and refresh the layers
        self.mem_point_layer.startEditing()
        # Checking whether the point was successfully added or not
        print(self.mem_point_layer.dataProvider().deleteFeatures(
            final_feat_list))
        self.mem_point_layer.commitChanges()
        self.mem_point_layer.updateExtents()
        self.iface.mapCanvas().refresh()

        # Disabling the point and radius option from using further
        self.dlg.point_radius_check.setDisabled(True)
        self.dlg.point_radius_frame.setDisabled(True)

    def settingMapTool(self):
        """Setting up the maptool
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        self.iface.mapCanvas().setMapTool(self.clickTool)

    def clearCanvas(self):
        """Clearing the previous selected point from the canvas
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        try:
            self.iface.mapCanvas().scene().removeItem(self.m)
        except AttributeError:
            pass

    def getMidPoint(self, point):
        """Get the point chosen by the user and display the same on the canvas
        :param point: THe point chosen by the user on the canvas
        :type point: QgsPoint

        :returns: None
        :rtype: None
        """
        self.clearCanvas()
        self.mx = point.x()
        self.my = point.y()
        self.iface.mapCanvas().unsetMapTool(self.clickTool)
        self.m = QgsVertexMarker(self.iface.mapCanvas())
        self.dlg.point_radius_frame_status.setText(
            "Selected Point: ({}, {})".format(self.mx, self.my))
        self.m.setCenter(QgsPoint(self.mx, self.my))

    def clearLayoutDock(self, layout):
        """Clears the extended options sroll area.
        :param layout: The layout of the second Scroll Area
        :type layout: QLayout

        :returns: None
        :rtype: None
        """
        for j in range(layout.count()):
            # Getting reference to the chechbox widget
            item = layout.takeAt(0).widget()
            # Setting the parent of the checkbox to Nonr
            item.setParent(None)
            # Removing the checkbox
            layout.removeWidget(item)
            # Deleting the widget
            item.deleteLater()

    def updateDataDictionary(self, opt, check):
        """Populates the ScrollArea for Storage Enabling on Hub basis
        :param opt: Option tells what to modify: technology(0) or storage(1)
        :type opt: Integer

        :param check: The checkbox object itself
        :type check: QCheckBox

        :param param: The Technology/Storage which needs to be modified
        :type param: String

        :returns: None
        :rtype: None
        """
        if(check.isChecked()):
            if(opt == 1):
                self.data_dictionary[int(check.text())][
                    u'Technologies'][self.dock_tab3_1.techCombo.currentText()][
                        u'Enabled'] = 1
            else:
                self.data_dictionary[int(check.text())][
                    u'Storages'][self.dock_tab3_1.storageCombo.currentText()][
                        u'Enabled'] = 1
        else:
            if(opt == 1):
                self.data_dictionary[int(check.text())][
                    u'Technologies'][self.dock_tab3_1.techCombo.currentText()][
                        u'Enabled'] = 0
            else:
                self.data_dictionary[int(check.text())][
                    u'Storages'][self.dock_tab3_1.storageCombo.currentText()][
                        u'Enabled'] = 0

    def updateDataDict(self, layout, opt, param):
        """Populates the ScrollArea for Storage Enabling on Hub basis
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        for j in range(layout.count()):
            # Getting reference to the chechbox widget
            item = layout.itemAt(0).widget()
            if(item.isChecked()):
                if(opt == 1):
                    self.data_dictionary[item.text(
                    )]["Technologies"][param]["Enabled"] = "1"
                elif(opt == 2):
                    self.data_dictionary[item.text(
                    )]["Storages"][param]["Enabled"] = "1"
            else:
                if(opt == 1):
                    self.data_dictionary[item.text(
                    )]["Technologies"][param]["Enabled"] = "0"
                elif(opt == 2):
                    self.data_dictionary[item.text(
                    )]["Storages"][param]["Enabled"] = "0"

    def updateHubsDockStorage(self):
        """Populates the ScrollArea for Storage Enabling on Hub basis
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        if(self.dock_tab3_1.storageCombo.currentText() == ""):
            # Clearing the layout first
            try:
                temp_layout = \
                    self.dock_tab3_1.scrollArea2_1DockContents.layout()
                self.clearLayoutDock(temp_layout)
                temp_layout.update()
            except Exception:
                return
            return

        # Clearing the layout first
        temp_layout = self.dock_tab3_1.scrollArea2_1DockContents.layout()
        self.clearLayoutDock(temp_layout)
        temp_layout.update()

        # Adding new items to the scrollArea
        for i in range(len(self.data_dictionary.keys())):
            temp_check = QCheckBox()
            temp_check.setText(str(self.data_dictionary.keys()[i]))
            if(self.data_dictionary[self.data_dictionary.keys(
            )[i]]["Storages"][self.dock_tab3_1.storageCombo.currentText(
            )]["Enabled"] == 1):
                # Make the Checkbox checked
                temp_check.setChecked(True)
            else:
                # Make the Checkbox un-checked
                temp_check.setChecked(False)
            temp_check.stateChanged.connect(partial(self.updateDataDictionary,
                                                    2, temp_check))
            temp_layout.addWidget(temp_check)

    def updateHubsDockTech(self):
        """Populates the ScrollArea for Technology Enabling on Hub basis
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        if(self.dock_tab3_1.techCombo.currentText() == ""):
            # Clearing the layout first
            try:
                temp_layout = \
                    self.dock_tab3_1.scrollArea1_1DockContents.layout()
                self.clearLayoutDock(temp_layout)
                temp_layout.update()
            except Exception:
                return
            return

        # Clearing the layout first
        temp_layout = self.dock_tab3_1.scrollArea1_1DockContents.layout()
        self.clearLayoutDock(temp_layout)
        temp_layout.update()

        # Adding new items to the scrollArea
        for i in range(len(self.data_dictionary)):
            temp_check = QCheckBox()
            temp_check.setText(str(self.data_dictionary.keys()[i]))
            if(self.data_dictionary[self.data_dictionary.keys(
            )[i]]["Technologies"][self.dock_tab3_1.techCombo.currentText(
            )]["Enabled"] == 1):
                # Make the Checkbox checked
                temp_check.setChecked(True)
            else:
                # Make the Checkbox un-checked
                temp_check.setChecked(False)
            temp_check.stateChanged.connect(partial(self.updateDataDictionary,
                                                    1, temp_check))
            temp_layout.addWidget(temp_check)

    def hubDockOkayButton(self):
        """Closes the dock and changes the TAB
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Closing the dock
        self.dock_tab3_1.close()

        # Disabling the plugin dialog box
        self.dlg.setDisabled(False)

        # Diable the Tab3 and enable Tab4 (you cannot navigate back)
        self.dlg.tabWidget.setCurrentIndex(3)
        self.dlg.tabWidget.setTabEnabled(3, True)
        self.dlg.tabWidget.setTabEnabled(2, False)

        # Disabling the second Radio Button if no road layer was loaded
        if(self.dlg.combo_road.currentText() == ''):
            self.dlg.radioTab3Opt2.setDisabled(True)
        self.dlg.radioTab3Opt1.setChecked(True)

        # Disabling the radius checkbox and corresponding frame
        self.dlg.point_radius_check.setChecked(False)
        self.dlg.point_radius_frame.setDisabled(True)

        # Setting the status box to read-only
        self.dlg.point_radius_frame_status.setReadOnly(True)

        # Initializing the spinbox
        self.dlg.radiusSpin.setRange(100, 10000)
        self.dlg.radiusSpin.setSingleStep(1)

    def getDistancesData(self):
        """Calculates and Stores the distances data for the steiner tree
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Creating data about length and end points
        expression = QgsExpression("$length")
        expression.prepare(self.mem_steiner_veclayer.pendingFields())

        prov = self.mem_steiner_veclayer.dataProvider()
        field1 = QgsField("length_km", QVariant.Double)
        prov.addAttributes([field1])
        self.mem_steiner_veclayer.updateFields()

        feat_itr = self.mem_steiner_veclayer.getFeatures()
        self.mem_steiner_veclayer.startEditing()
        for feat in feat_itr:
            value = expression.evaluate(feat)
            prov.changeAttributeValues(
                {feat.id(): {self.mem_steiner_veclayer.fieldNameIndex(
                    'length_km'): value}})

        self.mem_steiner_veclayer.commitChanges()
        self.mem_steiner_veclayer.updateExtents()
        self.iface.mapCanvas().refresh()

        self.my_dat = []

        vl = self.mem_steiner_veclayer
        layer = self.mem_point_layer

        director = QgsLineVectorLayerDirector(vl, -1, '', '', '', 3)
        properter = QgsDistanceArcProperter()
        director.addProperter(properter)
        crs = self.iface.mapCanvas().mapRenderer().destinationCrs()
        builder = QgsGraphBuilder(crs)

        point_list = []
        point_list_id = []
        features = layer.getFeatures()
        for fet in features:
            geom = fet.geometry()
            point_list.append(geom.asPoint())
            point_list_id.append(fet.attribute('id'))
        tiedPoints = director.makeGraph(builder, point_list)
        graph = builder.graph()

        for i in range(len(point_list)):
            tStart = tiedPoints[i]
            idStart = graph.findVertex(tStart)
            tree = []
            cost = []
            (tree, cost) = QgsGraphAnalyzer.dijkstra(graph, idStart, 0)
            for j in range(len(point_list)):
                if(j > i):
                    tStop = tiedPoints[j]
                    idStop = graph.findVertex(tStop)
                    if tree[idStop] == -1:
                        print("Path not found")
                    else:
                        dist = cost[idStop]
                        temp = [point_list_id[i], point_list_id[j], dist]
                        self.my_dat.append(temp)

    def saveOutput(self):
        """Saves the output in the final TAB
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        if(self.dlg.csvCheck.isChecked()):
            # Save in CSV format

            # Getting user defined filename
            if(self.dlg.saveFileName.text() == ''):
                text_save = ''
            else:
                text_save = self.dlg.saveFileName.text() + '_'

            with open(str(self.dlg.savePath.text()) +
                      '/' + text_save +
                      'General_export_price.csv', 'w') as csvfile:
                writer = csv.writer(csvfile, delimiter=',')
                temp_layout = self.dlg.exportPriceListContents.layout()
                temp_exp_list = []
                temp_exp_edit = []
                for i in range(temp_layout.count()):
                    temp_exp_list.append(
                        temp_layout.itemAt(i).widget().getLabel()[:-1])
                    temp_exp_edit.append(
                        temp_layout.itemAt(i).widget().getValue())
                for i in range(len(temp_exp_edit)):
                    writer.writerow([temp_exp_list[i], temp_exp_edit[i]])

            with open(str(self.dlg.savePath.text()) +
                      '/' + text_save +
                      'General_other.csv', 'w') as csvfile:
                writer = csv.writer(csvfile, delimiter=',')
                writer.writerow(["Interest Rate(%)", self.dlg.irEdit.text()])

            # Saving the Energy Network Data
            # with open(str(self.dlg.savePath.text()) +
            #           '/' + text_save +
            #           'Energy_Network.csv', 'w') as csvfile:
            #     writer = csv.writer(csvfile, delimiter=',')
            #     writer.writerow(['Line ID', 'Length(km)',
            #                      'Start Node', 'End Node'])
            #     feat_itr = self.mem_line_veclayer.getFeatures()
            #     for feat in feat_itr:
            #             atrs = feat.attributes()
            #             writer.writerow(atrs)
            if(self.network_flag == 1):
                with open(str(self.dlg.savePath.text()) +
                          '/' + text_save +
                          'Energy_Network_List.csv', 'w') as csvfile:
                    writer = csv.writer(csvfile, delimiter=',')
                    writer.writerow(['Line ID', 'Length(km)',
                                     'Start Node', 'End Node'])
                    feat_itr = self.mem_line_veclayer.getFeatures()
                    for feat in feat_itr:
                        atrs = feat.attributes()
                        writer.writerow(atrs)
                # =============================================================
                feat_itr = self.mem_line_veclayer.getFeatures()
                for feat in feat_itr:
                    atrs = feat.attributes()
                    self.temp_mat[int(atrs[2])][int(atrs[3])] = float(atrs[1])
                with open(str(self.dlg.savePath.text()) +
                          '/' + text_save +
                          'Energy_Network_Matrix.csv', 'w') as csvfile:
                    writer = csv.writer(csvfile, delimiter=',')
                    writer.writerows(self.temp_mat)
            elif(self.network_flag == 2):
                with open(str(self.dlg.savePath.text()) +
                          '/' + text_save +
                          'Energy_Network_List.csv', 'w') as csvfile:
                    writer = csv.writer(csvfile, delimiter=',')
                    writer.writerow(['Start Node', 'End Node', 'Length(m)'])
                    for i in range(len(self.my_dat)):
                        writer.writerow(self.my_dat[i])
                # =============================================================
                for i in range(len(self.my_dat)):
                    self.temp_mat[int(
                        self.my_dat[i][0])][int(
                            self.my_dat[i][1])] = float(self.my_dat[i][2])
                with open(str(self.dlg.savePath.text()) +
                          '/' + text_save +
                          'Energy_Network_Matrix.csv', 'w') as csvfile:
                    writer = csv.writer(csvfile, delimiter=',')
                    writer.writerows(self.temp_mat)

            # Saving the Technologies Data
            # Getting the complete Technology Paramters
            # and Carbon Factors List with all uniques labels
            list_full_param, list_full_carbon = self.getTechnologyValues()

            # Saving the Technology Parameters
            with open(str(self.dlg.savePath.text()) +
                      '/' + text_save +
                      'Technologies_Paramters.csv', 'w') as csvfile:
                writer = csv.writer(csvfile, delimiter=',')
                writer.writerow(["Name"] + self.scroll_1_list_tech_tab2)

                # Getting the data for the Technology Parameters
                for k in range(len(list_full_param)):
                    label = str(list_full_param[k])
                    temp = []
                    temp.append(label)
                    for i in range(self.dlg.technologiesList.count()):
                        flag = 0
                        for j in range(len(self.lineEdit_list_param_tab2[i])):
                            test_label = str(self.lineEdit_list_param_tab2[
                                i][j].getLabel()[:-1])
                            if(label == test_label):
                                flag = 1
                                if(self.lineEdit_list_param_tab2[
                                        i][j].getValue() == ""):
                                    temp.append(0)
                                else:
                                    temp.append(self.lineEdit_list_param_tab2[
                                            i][j].getValue())
                                break
                        if(flag == 0):
                            temp.append('NA')
                    writer.writerow(temp)

            # # Saving the Technology Factors
            # with open(str(self.dlg.savePath.text()) +
            #           '/' + text_save +
            #           'Technologies_CarbonFactors.csv', 'w') as csvfile:
            #     writer = csv.writer(csvfile, delimiter=',')
            #     writer.writerow(["Name"] + self.scroll_1_list_tech_tab2)
            #
            #     # Getting the data for the Carbon Factors
            #     for k in range(len(list_full_carbon)):
            #         label = str(list_full_carbon[k])
            #         temp = []
            #         temp.append(label)
            #         for i in range(self.dlg.technologiesList.count()):
            #             flag = 0
            #             for jinrange(len(self.lineEdit_list_carbon_tab2[i])):
            #                 test_label = str(self.lineEdit_list_carbon_tab2[
            #                     i][j].getLabel()[:-1])
            #                 if(label == test_label):
            #                     flag = 1
            #                     if(self.lineEdit_list_carbon_tab2[
            #                             i][j].getValue() == ""):
            #                         temp.append(0)
            #                     else:
            #                         temp.append(float(test_label))
            #                     break
            #             if(flag == 0):
            #                 temp.append('NA')
            #         writer.writerow(temp)

            # Saving the Technology Outputs
            with open(str(self.dlg.savePath.text()) +
                      '/' + text_save +
                      'Technologies_Outputs.csv', 'w') as csvfile:
                writer = csv.writer(csvfile, delimiter=',')
                writer.writerow(["Name"] + self.scroll_1_list_tech_tab2)

                # Getting the data for outputs
                for k in range(len(self.list_output)):
                    label = str(self.list_output[k])
                    temp = []
                    temp.append(label)
                    for i in range(self.dlg.technologiesList.count()):
                        flag = 0
                        for j in range(len(self.lineEdit_list_output_tab2[i])):
                            test_label = str(self.lineEdit_list_output_tab2[
                                i][j].getLabel()[:-1])
                            if(label == test_label):
                                flag = 1
                                if(self.lineEdit_list_output_tab2[
                                        i][j].getValue() == ""):
                                    temp.append(0)
                                else:
                                    temp.append(self.lineEdit_list_output_tab2[
                                            i][j].getValue())
                                break
                        if(flag == 0):
                            temp.append('NA')
                    writer.writerow(temp)

            # Saving the Storages Data
            with open(str(self.dlg.savePath.text()) +
                      '/' + text_save +
                      'Storages.csv', 'w') as csvfile:
                writer = csv.writer(csvfile, delimiter=',')
                writer.writerow(["Name"] + self.list_output)

                # Getting the data for the Storage Options
                list_full = self.getStorageOptionsList()
                for k in range(len(list_full)):
                    label = str(list_full[k])
                    temp = []
                    temp.append(label)
                    for i in range(len(self.list_output)):
                        flag = 0
                        for j in range(len(self.list_storage_param[i])):
                            test_label = str(self.list_storage_param[
                                i][j].getLabel()[:-1])
                            if(label == test_label):
                                flag = 1
                                if(self.list_storage_param[
                                        i][j].getValue() == ""):
                                    temp.append(0)
                                else:
                                    temp.append(self.list_storage_param[
                                            i][j].getValue())
                                    # temp.append(float(test_label))
                                break
                        if(flag == 0):
                            temp.append('NA')
                    writer.writerow(temp)

        if(self.dlg.excelCheck.isChecked()):
            # Save in excel format

            # Creating the excel workbook
            if(self.dlg.saveFileName.text() == ''):
                workbook = xlsxwriter.Workbook(
                    str(self.dlg.savePath.text()) +
                    '/General_input_MultiV2.xlsx')
            else:
                workbook = xlsxwriter.Workbook(
                    str(self.dlg.savePath.text()) +
                    '/' + self.dlg.saveFileName.text() + '.xlsx')
            bold = workbook.add_format({'bold': True})

            # Making and Saving the general data Worksheet
            worksheet = workbook.add_worksheet('General')
            row = 0
            col = 0
            worksheet.write(row, col, 'General Data', bold)
            row = row + 2
            worksheet.write(row, 0, "Interest Rate(%)")
            worksheet.write(row, 1, self.dlg.irEdit.text())
            row = row + 4
            worksheet.write(row, 0, "")
            worksheet.write(row, 1, "Export Price(CHF/kWh)", bold)
            row = row + 1
            temp_layout = self.dlg.exportPriceListContents.layout()
            temp_exp_list = []
            temp_exp_edit = []
            for i in range(temp_layout.count()):
                temp_exp_list.append(
                    temp_layout.itemAt(i).widget().getLabel()[:-1])
                temp_exp_edit.append(temp_layout.itemAt(i).widget().getValue())
            for i in range(len(temp_exp_edit)):
                worksheet.write(row, 0, temp_exp_list[i])
                worksheet.write(row, 1, temp_exp_edit[i])
                row = row + 1

            # ============================================================
            if(self.network_flag == 1):
                worksheet = workbook.add_worksheet('Network List')
                row = 0
                col = 0
                worksheet.write(row, col, 'Energy Network', bold)
                row = row + 1
                header = ['Line ID', 'Length(km)', 'Start Node', 'End Node']
                for i in range(len(header)):
                    worksheet.write(row, i, header[i], bold)
                row = row + 1
                feat_itr = self.mem_line_veclayer.getFeatures()
                for feat in feat_itr:
                    atrs = feat.attributes()
                    for i in range(len(atrs)):
                        worksheet.write(row, i, atrs[i])
                    row = row + 1
                # ============================================================
                feat_itr = self.mem_line_veclayer.getFeatures()
                for feat in feat_itr:
                    atrs = feat.attributes()
                    self.temp_mat[int(atrs[2])][int(atrs[3])] = float(atrs[1])
                worksheet = workbook.add_worksheet('Network Matrix')
                row = 0
                col = 1
                for i in range(len(self.temp_mat)):
                    worksheet.write(row, col, i)
                    col = col + 1
                row = 1
                col = 0
                for i in range(len(self.temp_mat)):
                    worksheet.write(row, col, i)
                    row = row + 1
                row = 1
                col = 1
                for i in range(len(self.temp_mat)):
                    for j in range(len(self.temp_mat)):
                        worksheet.write(row, col, self.temp_mat[i][j])
                        col = col + 1
                    row = row + 1
                    col = 1
            elif(self.network_flag == 2):
                worksheet = workbook.add_worksheet('Network List')
                row = 0
                col = 0
                worksheet.write(row, col, 'Energy Network', bold)
                row = row + 1
                header = ['Start Node', 'End Node', 'Length(m)']
                for i in range(len(header)):
                    worksheet.write(row, i, header[i], bold)
                row = row + 1
                for i in range(len(self.my_dat)):
                    for j in range(len(header)):
                        worksheet.write(row, j, self.my_dat[i][j])
                    row = row + 1
                # ============================================================
                for i in range(len(self.my_dat)):
                    self.temp_mat[int(
                        self.my_dat[i][0])][int(
                            self.my_dat[i][1])] = float(self.my_dat[i][2])
                worksheet = workbook.add_worksheet('Network Matrix')
                row = 0
                col = 1
                for i in range(len(self.temp_mat)):
                    worksheet.write(row, col, i)
                    col = col + 1
                row = 1
                col = 0
                for i in range(len(self.temp_mat)):
                    worksheet.write(row, col, i)
                    row = row + 1
                row = 1
                col = 1
                for i in range(len(self.temp_mat)):
                    for j in range(len(self.temp_mat)):
                        worksheet.write(row, col, self.temp_mat[i][j])
                        col = col + 1
                    row = row + 1
                    col = 1
            # ============================================================

            # ============================================================
            worksheet = workbook.add_worksheet('Technology_Dispatchable')
            row = 0
            col = 0
            worksheet.write(row, col, 'Technologies', bold)
            row = row + 1
            temp_header = [""] + self.scroll_1_list_tech_tab2

            # Indexes which correspond to colmns with Solar Data
            index_solar = []
            temp_header_solar = [""]
            for i in range(len(temp_header)):
                if(temp_header[i][-7:] == '(Solar)'):
                    index_solar.append(i)
                    temp_header_solar.append(temp_header[i])

            col = -1
            for i in range(len(temp_header)):
                if(i == 0 or i not in index_solar):
                    col = col + 1
                    worksheet.write(row, col, temp_header[i], bold)
            row = row + 1
            list_full_param, list_full_carbon = self.getTechnologyValues()
            # Getting the data for the Technology Parameters
            for k in range(len(list_full_param)):
                label = str(list_full_param[k])
                temp = []
                temp.append(label)
                for i in range(self.dlg.technologiesList.count()):
                    flag = 0
                    for j in range(len(self.lineEdit_list_param_tab2[i])):
                        test_label = str(self.lineEdit_list_param_tab2[
                            i][j].getLabel()[:-1])
                        if(label == test_label):
                            flag = 1
                            if(self.lineEdit_list_param_tab2[
                                    i][j].getValue() == ""):
                                temp.append(0)
                            else:
                                # temp.append(float(test_label))
                                temp.append(self.lineEdit_list_param_tab2[
                                        i][j].getValue())
                            break
                    if(flag == 0):
                        temp.append('NA')
                col = -1
                for l in range(len(temp)):
                    if(l == 0 or l not in index_solar):
                        col = col + 1
                        worksheet.write(row, col, temp[l])
                row = row + 1
            # ============================================================

            # ============================================================
            row = row + 2
            col = 0
            worksheet.write(row, col, 'Output', bold)
            row = row + 1
            temp_header = [""] + self.scroll_1_list_tech_tab2
            col = -1
            for i in range(len(temp_header)):
                if(i == 0 or i not in index_solar):
                    col = col + 1
                    worksheet.write(row, col, temp_header[i], bold)
            row = row + 1
            # Getting the data for outputs
            for k in range(len(self.list_output)):
                label = str(self.list_output[k])
                temp = []
                temp.append(label)
                for i in range(self.dlg.technologiesList.count()):
                    flag = 0
                    for j in range(len(self.lineEdit_list_output_tab2[i])):
                        test_label = str(self.lineEdit_list_output_tab2[
                            i][j].getLabel()[:-1])
                        if(label == test_label):
                            flag = 1
                            if(self.lineEdit_list_output_tab2[
                                    i][j].getValue() == ""):
                                temp.append(0)
                            else:
                                # temp.append(float(test_label))
                                temp.append(self.lineEdit_list_output_tab2[
                                        i][j].getValue())
                            break
                    if(flag == 0):
                        temp.append('NA')
                col = -1
                for l in range(len(temp)):
                    if(l == 0 or l not in index_solar):
                        col = col + 1
                        worksheet.write(row, col, temp[l])
                row = row + 1
            # ============================================================

            # ============================================================
            # row = row + 2
            # col = 0
            # worksheet.write(row, col, 'Technology Carbon Factors', bold)
            # row = row + 1
            # temp_header = [""] + self.scroll_1_list_tech_tab2
            # col = -1
            # for i in range(len(temp_header)):
            #     if(i == 0 or i not in index_solar):
            #         col = col + 1
            #         worksheet.write(row, col, temp_header[i], bold)
            # row = row + 1
            # # Getting the data for the Carbon Factors
            # for k in range(len(list_full_carbon)):
            #     label = str(list_full_carbon[k])
            #     temp = []
            #     temp.append(label)
            #     for i in range(self.dlg.technologiesList.count()):
            #         flag = 0
            #         for j in range(len(self.lineEdit_list_carbon_tab2[i])):
            #             test_label = str(self.lineEdit_list_carbon_tab2[
            #                 i][j].getLabel()[:-1])
            #             if(label == test_label):
            #                 flag = 1
            #                 if(self.lineEdit_list_carbon_tab2[
            #                         i][j].getValue() == ""):
            #                     temp.append(0)
            #                 else:
            #                     # temp.append(float(test_label))
            #                     temp.append(self.lineEdit_list_carbon_tab2[
            #                             i][j].getValue())
            #                 break
            #         if(flag == 0):
            #             temp.append('NA')
            #     col = -1
            #     for l in range(len(temp)):
            #         if(l == 0 or l not in index_solar):
            #             col = col + 1
            #             worksheet.write(row, col, temp[l])
            #     row = row + 1
            # ============================================================

            # ============================================================
            worksheet = workbook.add_worksheet('Technology_Solar')
            row = 0
            col = 0
            worksheet.write(row, col, 'Technologies', bold)
            row = row + 1
            for i in range(len(temp_header_solar)):
                worksheet.write(row, i, temp_header_solar[i], bold)
            row = row + 1

            list_full_param, list_full_carbon = self.getTechnologyValues()
            # Getting the data for the Technology Parameters
            for k in range(len(list_full_param)):
                label = str(list_full_param[k])
                temp = []
                temp.append(label)
                for i in range(self.dlg.technologiesList.count()):
                    flag = 0
                    for j in range(len(self.lineEdit_list_param_tab2[i])):
                        test_label = str(self.lineEdit_list_param_tab2[
                            i][j].getLabel()[:-1])
                        if(label == test_label):
                            flag = 1
                            if(self.lineEdit_list_param_tab2[
                                    i][j].getValue() == ""):
                                temp.append(0)
                            else:
                                # temp.append(float(test_label))
                                temp.append(self.lineEdit_list_param_tab2[
                                        i][j].getValue())
                            break
                    if(flag == 0):
                        temp.append('NA')
                col = -1
                for l in range(len(temp)):
                    if(l == 0 or l in index_solar):
                        col = col + 1
                        worksheet.write(row, col, temp[l])
                row = row + 1

            row = row + 2
            col = 0
            worksheet.write(row, col, 'Output', bold)
            row = row + 1
            temp_header = [""] + self.scroll_1_list_tech_tab2
            for i in range(len(temp_header_solar)):
                worksheet.write(row, i, temp_header_solar[i], bold)
            row = row + 1
            # Getting the data for outputs
            for k in range(len(self.list_output)):
                label = str(self.list_output[k])
                temp = []
                temp.append(label)
                for i in range(self.dlg.technologiesList.count()):
                    flag = 0
                    for j in range(len(self.lineEdit_list_output_tab2[i])):
                        test_label = str(self.lineEdit_list_output_tab2[
                            i][j].getLabel()[:-1])
                        if(label == test_label):
                            flag = 1
                            if(self.lineEdit_list_output_tab2[
                                    i][j].getValue() == ""):
                                temp.append(0)
                            else:
                                # temp.append(float(test_label))
                                temp.append(self.lineEdit_list_output_tab2[
                                        i][j].getValue())
                            break
                    if(flag == 0):
                        temp.append('NA')
                col = -1
                for l in range(len(temp)):
                    if(l == 0 or l in index_solar):
                        col = col + 1
                        worksheet.write(row, col, temp[l])
                row = row + 1

            # row = row + 2
            # col = 0
            # worksheet.write(row, col, 'Technology Carbon Factors', bold)
            # row = row + 1
            # temp_header = [""] + self.scroll_1_list_tech_tab2
            # for i in range(len(temp_header_solar)):
            #     worksheet.write(row, i, temp_header_solar[i], bold)
            # row = row + 1
            # # Getting the data for the Carbon Factors
            # for k in range(len(list_full_carbon)):
            #     label = str(list_full_carbon[k])
            #     temp = []
            #     temp.append(label)
            #     for i in range(self.dlg.technologiesList.count()):
            #         flag = 0
            #         for j in range(len(self.lineEdit_list_carbon_tab2[i])):
            #             test_label = str(self.lineEdit_list_carbon_tab2[
            #                 i][j].getLabel()[:-1])
            #             if(label == test_label):
            #                 flag = 1
            #                 if(self.lineEdit_list_carbon_tab2[
            #                         i][j].getValue() == ""):
            #                     temp.append(0)
            #                 else:
            #                     # temp.append(float(test_label))
            #                     temp.append(self.lineEdit_list_carbon_tab2[
            #                             i][j].getValue())
            #                 break
            #         if(flag == 0):
            #             temp.append('NA')
            #     col = -1
            #     for l in range(len(temp)):
            #         if(l == 0 or l in index_solar):
            #             col = col + 1
            #             worksheet.write(row, col, temp[l])
            #     row = row + 1
            # ============================================================

            # ============================================================
            worksheet = workbook.add_worksheet('Technologies_Storage')
            row = 0
            col = 0
            worksheet.write(row, col, 'Storages', bold)
            row = row + 1
            temp_header = [""] + self.list_output
            for i in range(len(temp_header)):
                worksheet.write(row, i, temp_header[i], bold)
            row = row + 1
            # Getting the data for the Storage Options
            list_full = self.getStorageOptionsList()
            for k in range(len(list_full)):
                label = str(list_full[k])
                temp = []
                temp.append(label)
                for i in range(len(self.list_output)):
                    flag = 0
                    for j in range(len(self.list_storage_param[i])):
                        test_label = str(self.list_storage_param[
                            i][j].getLabel()[:-1])
                        if(label == test_label):
                            flag = 1
                            if(self.list_storage_param[
                                    i][j].getValue() == ""):
                                temp.append(0)
                            else:
                                # temp.append(float(test_label))
                                temp.append(self.list_storage_param[
                                        i][j].getValue())
                            break
                    if(flag == 0):
                        temp.append('NA')
                for l in range(len(temp)):
                    worksheet.write(row, l, temp[l])
                row = row + 1
            # ============================================================

            # Closing the workbook
            workbook.close()

    def open_file_path(self):
        """Get the path of the CSV/Excel file to read demand data
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        self.input_demand_data_path = QFileDialog.getOpenFileName(
            self.dlg, "Open File",  "/home",
            "Excel files (*.xlsx)")
        self.dlg.openPath.setText(self.input_demand_data_path)

    def save_directory_path(self):
        """Get the path of the output directory where data needs to be stored
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        output_dir = QFileDialog.getExistingDirectory(
            self.dlg, "Select output directory ", "")
        self.dlg.savePath.setText(output_dir)

    def savePathEdit(self):
        """Controls the Save Button
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        if(self.dlg.savePath.text() == ""):
            self.dlg.saveButton.setDisabled(True)
        else:
            self.dlg.saveButton.setDisabled(False)

    def issueWarning(self, tab_num, flag_val):
        """Issue warning to the user in case any of the data is incomplete
        :param None:
        :type None:

        :returns Status: The
        :rtype: Integer
        """
        if(tab_num == 1):

            if(self.dlg.irEdit.text() == '' or
               self.dlg.openPath.text() == '' or
               self.dlg.expEdit3.text() == '' or
               self.dlg.expEdit2.text() == '' or
               self.dlg.expEdit1.text() == ''):
                if(flag_val == 0):
                    self.iface.messageBar().pushCritical(
                        "Incomplete Data",
                        "Some Data Fields are empty! Click Proceed Again")
                    return 0
                else:
                    self.iface.messageBar().pushCritical(
                        "Incomplete Data",
                        "You Left the data empty empty!")
                    return 1
            else:
                self.iface.messageBar().pushSuccess(
                    "Data Complete",
                    "All the required data has been Entered!")
                return 1

        elif(tab_num == 2):
            empty_flag = 0

            # Checking for empty text edit in ScrollBox 1
            for i in range(len(self.lineEdit_list_param_tab2)):
                for j in range(len(self.lineEdit_list_param_tab2[i])):
                    if(self.lineEdit_list_param_tab2[i][j].isEmpty()):
                        empty_flag = 1
                        break

            if(empty_flag == 0):
                # Checking for empty text edit in ScrollBox 2
                for i in range(len(self.lineEdit_list_output_tab2)):
                    for j in range(len(self.lineEdit_list_output_tab2[i])):
                        if(self.lineEdit_list_output_tab2[i][j].isEmpty()):
                            empty_flag = 1
                            break

            if(empty_flag):
                if(flag_val == 0):
                    self.iface.messageBar().pushCritical(
                        "Incomplete Data",
                        "Some Data Fields are empty! Click Proceed Again")
                    return 0
                else:
                    self.iface.messageBar().pushCritical(
                        "Incomplete Data",
                        "You Left the data empty empty!")
                    return 1
            else:
                self.iface.messageBar().pushSuccess(
                    "Data Complete",
                    "All the required data has been Entered!")
                return 1

        elif(tab_num == 3):
            empty_flag = 0

            # Checking for empty text edit in ScrollBox TAB3
            for i in range(len(self.list_storage_param)):
                for j in range(len(self.list_storage_param[i])):
                    if(self.list_storage_param[i][j].isEmpty()):
                        empty_flag = 1
                        break

            if(empty_flag):
                if(flag_val == 0):
                    self.iface.messageBar().pushCritical(
                        "Incomplete Data",
                        "Some Data Fields are empty! Click Proceed Again")
                    return 0
                else:
                    self.iface.messageBar().pushCritical(
                        "Incomplete Data",
                        "You Left the data empty empty!")
                    return 1
            else:
                self.iface.messageBar().pushSuccess(
                    "Data Complete",
                    "All the required data has been Entered!")
                return 1

    def defineSignals(self):
        """Defines the global signals used throughout the code
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Connecting the signals whenever a layer is added or removed
        QgsMapLayerRegistry.instance().layersRemoved.connect(
            self.updateComboBox)
        QgsMapLayerRegistry.instance().layersAdded.connect(self.updateComboBox)

        # Signals of TAB1
        self.dlg.toolButtonTab1.clicked.connect(self.open_file_path)
        # Connecting the Proceed Button of TAB1
        self.dlg.proceedButton1.clicked.connect(self.proceedTab1)

        # Connecting the Add, Remove and Edit Buttons of TAB1
        # Signals for the Add Output button of TAB1
        self.dlg.addButtonTab1.clicked.connect(self.addOutputTab1)
        self.add_dlg_tab1.backButton.clicked.connect(partial(
            self.returnAddButtonTab1, 0))
        self.add_dlg_tab1.okButton.clicked.connect(partial(
            self.returnAddButtonTab1, 1))
        self.add_dlg_tab1.name.textChanged.connect(partial(
            self.buttonControl, self.add_dlg_tab1.okButton,
            self.add_dlg_tab1.name, None))

        # Signals for the Remove Output button of TAB1
        self.dlg.removeButtonTab1.clicked.connect(self.removeOutputTab1)
        self.remove_dlg_tab1.backButton.clicked.connect(partial(
            self.returnRemoveButtonTab1, 0))
        self.remove_dlg_tab1.okButton.clicked.connect(partial(
            self.returnRemoveButtonTab1, 1))

        # TODO (ISSUE): logic to make sure that new addition is not a duplicate
        # for both, a new storage option or the name of technology itself

        # TODO (ISSUE): Work with multiple selections

        # TODO (ISSUE): Merge the methods: self.moveButtonEditDialogTab1
        # and self.moveButtonAddDialogTab1

        # TODO (ISSUE): On the Edit tab work for persistence such that
        # multiple technolgy output edits can be accomodated at once

        # TODO (ISSUE): Think about data persistence i.e. how and when to
        # write the metadata changes in the internal json files

        # Signals of TAB2
        # Signal for the ListWidget on TAB2
        self.dlg.technologiesList.itemClicked.connect(self.updateScrollsTab2)

        # Connecting the Add, Remove and Edit Buttons of TAB2
        # Signals for the Add Technology button of TAB2
        self.dlg.addButtonTab2.clicked.connect(self.addTechnologyTab2)
        self.add_dlg_tab2.backButton.clicked.connect(partial(
            self.returnAddButtonTab2, 0))
        self.add_dlg_tab2.okButton.clicked.connect(partial(
            self.returnAddButtonTab2, 1))
        self.add_dlg_tab2.addNewEdit1.textChanged.connect(partial(
            self.buttonControl, self.add_dlg_tab2.addNewButton1,
            self.add_dlg_tab2.addNewEdit1, None))
        # self.add_dlg_tab2.addNewEdit2.textChanged.connect(partial(
        #     self.buttonControl, self.add_dlg_tab2.addNewButton2,
        #     self.add_dlg_tab2.addNewEdit2, None))
        self.add_dlg_tab2.name.textChanged.connect(partial(
            self.nameAddEnableAddTab2))
        self.add_dlg_tab2.moveLeftButton1.clicked.connect(partial(
            self.moveButtonAddDialogTab2, 1, self.add_dlg_tab2.list_11,
            self.add_dlg_tab2.list_21))
        self.add_dlg_tab2.moveRightButton1.clicked.connect(partial(
            self.moveButtonAddDialogTab2, 0, self.add_dlg_tab2.list_11,
            self.add_dlg_tab2.list_21))
        # self.add_dlg_tab2.moveLeftButton2.clicked.connect(partial(
        #     self.moveButtonAddDialogTab2, 1, self.add_dlg_tab2.list_12,
        #     self.add_dlg_tab2.list_22))
        # self.add_dlg_tab2.moveRightButton2.clicked.connect(partial(
        #     self.moveButtonAddDialogTab2, 0, self.add_dlg_tab2.list_12,
        #     self.add_dlg_tab2.list_22))
        self.add_dlg_tab2.addNewButton1.clicked.connect(partial(
            self.addNewTechParamAddTab2))
        # self.add_dlg_tab2.addNewButton2.clicked.connect(partial(
        #     self.addNewTechCarbonAddTab2))

        # Signals for the Remove Technology button of TAB2
        self.dlg.removeButtonTab2.clicked.connect(self.removeTechnologyTab2)
        self.remove_dlg_tab2.backButton.clicked.connect(partial(
            self.returnRemoveButtonTab2, 0))
        self.remove_dlg_tab2.okButton.clicked.connect(partial(
            self.returnRemoveButtonTab2, 1))

        # Signals for the Edit Technology button of TAB2
        # # self.dlg.edit_dialog_tab2.clicked.connect(self.editTechnologyTab2)
        self.dlg.editButtonTab2.clicked.connect(self.editTechnologyTab2)
        self.edit_dlg_tab2.backButton.clicked.connect(partial(
            self.returnEditButtonTab2, 0))
        self.edit_dlg_tab2.okButton.clicked.connect(partial(
            self.returnEditButtonTab2, 1))
        self.edit_dlg_tab2.outputCombo.currentIndexChanged.connect(partial(
            self.updateListsEditTab2))
        self.edit_dlg_tab2.addNewEdit1.textChanged.connect(partial(
            self.buttonControl, self.edit_dlg_tab2.addNewButton1,
            self.edit_dlg_tab2.addNewEdit1, None))
        # self.edit_dlg_tab2.addNewEdit2.textChanged.connect(partial(
        #     self.buttonControl, self.edit_dlg_tab2.addNewButton2,
        #     self.edit_dlg_tab2.addNewEdit2, None))
        self.edit_dlg_tab2.moveLeftButton1.clicked.connect(partial(
            self.moveButtonEditDialogTab2, 1, self.edit_dlg_tab2.list_11,
            self.edit_dlg_tab2.list_21))
        self.edit_dlg_tab2.moveRightButton1.clicked.connect(partial(
            self.moveButtonEditDialogTab2, 0, self.edit_dlg_tab2.list_11,
            self.edit_dlg_tab2.list_21))
        # self.edit_dlg_tab2.moveLeftButton2.clicked.connect(partial(
        #     self.moveButtonEditDialogTab2, 1, self.edit_dlg_tab2.list_12,
        #     self.edit_dlg_tab2.list_22))
        # self.edit_dlg_tab2.moveRightButton2.clicked.connect(partial(
        #     self.moveButtonEditDialogTab2, 0, self.edit_dlg_tab2.list_12,
        #     self.edit_dlg_tab2.list_22))
        self.edit_dlg_tab2.addNewButton1.clicked.connect(partial(
            self.addNewTechParamEditTab2))
        # self.edit_dlg_tab2.addNewButton2.clicked.connect(partial(
        #     self.addNewTechCarbonEditTab2))

        # Connecting the Proceed Button of TAB2
        self.dlg.proceedButton2.clicked.connect(self.proceedTab2)

        # Signals of TAB3
        # Signal for the ListWidget of TAB3
        self.dlg.outputsListTab3.itemClicked.connect(
            self.updateStorageListTab3)

        # Signals for the Edit button and dialog box of TAB3
        self.dlg.editButtonTab3.clicked.connect(self.editOutputTab3)
        self.edit_dlg_tab3.backButton.clicked.connect(partial(
            self.returnEditButtonTab3, 0))
        self.edit_dlg_tab3.okButton.clicked.connect(partial(
            self.returnEditButtonTab3, 1))
        self.edit_dlg_tab3.outputCombo.currentIndexChanged.connect(partial(
            self.updateListsEditTab3))
        self.edit_dlg_tab3.addNewEdit.textChanged.connect(partial(
            self.buttonControl, self.edit_dlg_tab3.addNewButton,
            self.edit_dlg_tab3.addNewEdit, None))
        self.edit_dlg_tab3.moveLeftButton.clicked.connect(partial(
            self.moveButtonAddDialogTab3, 1, self.edit_dlg_tab3.list_1,
            self.edit_dlg_tab3.list_2))
        self.edit_dlg_tab3.moveRightButton.clicked.connect(partial(
            self.moveButtonAddDialogTab3, 0, self.edit_dlg_tab3.list_1,
            self.edit_dlg_tab3.list_2))
        self.edit_dlg_tab3.addNewButton.clicked.connect(partial(
            self.addNewStorageOptionEditTab3))

        # Connecting the Proceed Button of TAB3
        self.dlg.proceedButton3.clicked.connect(self.proceedTab3)

        # Signals of DockWidget1 of TAB3
        # COnnecting the signals for the Combo Boxes
        self.dock_tab3_1.techCombo.currentIndexChanged.connect(partial(
            self.updateHubsDockTech))
        self.dock_tab3_1.storageCombo.currentIndexChanged.connect(partial(
            self.updateHubsDockStorage))
        self.dock_tab3_1.okayButton.clicked.connect(self.hubDockOkayButton)

        # Signals of TAB4
        # Connecting the confirm button on TAB4
        self.dlg.confirmButtonTab4.clicked.connect(self.confirmTab4)
        self.dlg.radioTab3Opt2.toggled.connect(self.statusUpdateTab4)
        self.dlg.comboBoxDirectMethod.currentIndexChanged.connect(
            self.statusUpdateTab4)

        # Signals of DockWidget1 of TAB4
        # COnnecting the signals for the radio buttons
        self.dock_tab4_1.useLineRadio.toggled.connect(partial(
            self.radioControl, self.dock_tab4_1.usePointRadio,
            self.dock_tab4_1.useLineRadio, self.dock_tab4_1.frame_1,
            self.dock_tab4_1.frame_2))
        self.dock_tab4_1.useLineRadio.toggled.connect(partial(
            self.radioControl, self.dock_tab4_1.usePointRadio,
            self.dock_tab4_1.useLineRadio, self.dock_tab4_1.frame_1,
            self.dock_tab4_1.frame_2))
        self.dock_tab4_1.addOkButton.clicked.connect(partial(
            self.dockOkButtonControl, 1, self.dock_tab4_1.addSrcPointCombo,
            self.dock_tab4_1.addDstPointCombo, self.dock_tab4_1.statusBox))
        self.dock_tab4_1.removeOkButton1.clicked.connect(partial(
            self.dockOkButtonControl, 2, self.dock_tab4_1.removeSrcPointCombo,
            self.dock_tab4_1.removeDstPointCombo, self.dock_tab4_1.statusBox))
        self.dock_tab4_1.removeOkButton2.clicked.connect(partial(
            self.dockOkButtonControl, 3, self.dock_tab4_1.lineCombo,
            None, self.dock_tab4_1.statusBox))
        self.dock_tab4_1.proceedButton.clicked.connect(self.proceedDockTab4)

        # Signals used for getting the point on canvas and filter with radius
        self.dlg.point_radius_check.stateChanged.connect(self.pointRadiusCheck)
        self.dlg.selectPointButton.clicked.connect(self.settingMapTool)
        self.dlg.confirmPointsButton.clicked.connect(self.getPoints)
        QObject.connect(self.clickTool, SIGNAL(
            "canvasClicked(const QgsPoint&, Qt::MouseButton)"),
                        self.getMidPoint)

        # Signals of TAB5
        # Signal for Save Button
        self.dlg.saveButton.clicked.connect(self.saveOutput)

        # Signal for the LineEdit that controls the Save Button
        self.dlg.savePath.textChanged.connect(self.savePathEdit)

        # Signal for Tool Button
        self.dlg.toolButton.clicked.connect(self.save_directory_path)

        # ==TST
        # self.dlg.testbt.clicked.connect(self.selectPoint)
        # ==TST (below)
        # self.dlg.testbt.clicked.connect(self.testingpass)
        # ==TST (above)
        # self.mapTool = IdentifyGeometry(self.iface.mapCanvas())
        # self.mapTool.geomIdentified.connect(self.editFeature)
        # self.iface.mapCanvas().setMapTool(self.mapTool)

    def getOutputList(self):
        """Get the data from the output JSON file and return it
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        with open(self.output_tech_file_path) as data_file:
            data = json.load(data_file)
        return data.keys(), [data[data.keys()[i]] for i in range(len(data))]

    def tab1EditChanged(self):
        """Handler for all the LabelEdit in Tab1
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        proceed_tab1_flag = 0
        # Disabling proceed button if all the parameters are not filled
        for i in range(len(self.list_storage_param)):
            for j in range(len(self.list_storage_param[i])):
                if(self.list_storage_param[i][j].isEmpty()):
                        proceed_tab1_flag = 2
                        break
        self.proceedButtonTab1Status(proceed_tab1_flag)

    def getLineEditList(self, list_label):
        """Return the list of LineEdit boxes for
           the list specified as the parameter
        :param list_label: The list of labels for which line edit is to be made
        :type list_label: List

        :returns: The list of the LineEdits
        :rtype: List
        """
        temp1 = []
        for i in range(len(list_label)):
            temp2 = []
            for j in range(len(list_label[i])):
                temp_label_edit = LabelEdit(
                    list_label[i][j]+str(":"), 2, 3)
                temp_label_edit.edit.textChanged.connect(partial(
                    self.tab1EditChanged))
                temp2.append(temp_label_edit)
            temp1.append(temp2)
        return temp1

    def proceedButtonTab1Status(self, value):
        """COntrols the Proceed Button of TAB1 and the Status Label
        :param value: Controls the button and setText of label based on value
        :type value:

        :returns: None
        :rtype: None
        """
        if(value == 0):
            self.dlg.proceedButton1.setDisabled(False)
            self.dlg.status_label.setText("Proceed to further Steps.")
        else:
            self.dlg.proceedButton1.setDisabled(True)
            if(value == 1):
                self.dlg.status_label.setText(
                    "Atleast one Technology Output is required")
            elif(value == 2):
                self.dlg.status_label.setText(
                    "All the storge parameter values must be filled")

    def initializeListWidget(self):
        """Initializes the LIst Widget with output options
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Get the list of technologies from the CSV file
        self.output_tech_file_path = os.path.join(self.plugin_dir,
                                                  self.filename_output)
        self.list_output, self.list_output_param = self.getOutputList()

        # Adding items to the list widget
        self.dlg.outputsList.clear()
        self.dlg.outputsList.addItems(self.list_output)

        # Getting the lineEdits for all the params
        self.list_storage_param = self.getLineEditList(self.list_output_param)

        # TAB1 Scroll Area Initialization
        self.scroll_1_layout_tab1 = QVBoxLayout()
        self.dlg.exportPriceListContents.setLayout(self.scroll_1_layout_tab1)
        temp_layout = self.dlg.exportPriceListContents.layout()
        temp1 = []
        for i in range(len(self.list_output)):
            temp_label_edit = LabelEdit(
                self.list_output[i]+str(":"), 2, 3)
            temp1.append(temp_label_edit)
        for i in range(len(self.list_output)):
            temp_layout.addWidget(temp1[i])

        # Disabling proceed button if list empty
        if(self.dlg.outputsList.count() == 0):
            self.dlg.proceedButton1.setDisabled(True)

        # Disabling proceed button if all the parameters are not filled
        # for i in range(len(self.list_storage_param)):
        #     for j in range(len(self.list_storage_param[i])):
        #         if(self.list_storage_param[i][j].isEmpty()):
        #                 proceed_tab1_flag = 2
        #                 break
        # self.proceedButtonTab1Status(proceed_tab1_flag)

    def initializeGUI(self):
        """Initializes the GUI elements and populates them
        :param None:
        :type None:

        :returns: None
        :rtype: None
        """
        # Initial widget actions when just opened
        # Defining all the signals present in the plugin
        self.defineSignals()

        # Checking if the Tab1 should be enabled or diabled
        self.updateComboBox()

        # Disabling the EditText
        self.dlg.openPath.setReadOnly(True)

        # Updating the comboboxes dyamically based on loading/unloading layers
        self.dlg.combo_building.setFilters(QgsMapLayerProxyModel.PolygonLayer)
        self.dlg.combo_road.setFilters(QgsMapLayerProxyModel.LineLayer)

        # Populating the ListWidget in TAB1
        self.initializeListWidget()

        # Disabling all the tabs
        self.disableTabs()

    def run(self):
        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # Removing the point which was selected on the map canvas
        self.clearCanvas()
        # See if OK was pressed
        # ==TST
        # self.emitPoint = QgsMapToolEmitPoint(self.iface.mapCanvas())
        # QObject.connect(self.emitPoint, SIGNAL("canvasClicked(const QgsPoint
        # &, Qt::MouseButton)"), self.clickedOnMap)
        if result:
            # Do something useful here - delete the line containing pass and
            # substitute with your code.
            pass

# TODO: after closing it should reload
