# -*- coding: utf-8 -*-
"""
/***************************************************************************
 EnergyHub
                                 A QGIS plugin
 QGIS plugin for HUES platform, eHub Model
                             -----------------------------
        begin                : 2017-06-17
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Rachit Kansal
        email                : rachitkansal1995@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtGui import QWidget, QLabel, QLineEdit, QHBoxLayout, \
    QDoubleValidator


class LabelEdit(QWidget):
    """Custom LineEdit Implementation:
    LineEdit includes a Label builtin and also takes only numerical inputs"""
    def __init__(self, label, wt1=None, wt2=None):
        """Constructor.

        :param label: The label for the LineEdit
        :type label: String

        :param wt1: Stretch factor for the Label
        :type wt1: Integer

        :param wt2: Stretch factor for the LineEdit
        :type wt2: Integer

        :returns: None
        :rtype: None
        """
        super(LabelEdit, self).__init__()
        # Making the label
        self.label = QLabel(label)
        # Making the lineEdit
        self.edit = QLineEdit()
        self.edit.setValidator(QDoubleValidator(0, 100, 2))
        # Setting up the Layout
        layout = QHBoxLayout()
        # Setting the Label with or wtihout stretch
        if wt1 is not None:
            layout.addWidget(self.label, wt1)
        else:
            layout.addWidget(self.label)
        # Setting the LineEdit with or wtihout stretch
        if wt2 is not None:
            layout.addWidget(self.edit, wt2)
        else:
            layout.addWidget(self.edit)
        # Default margin for the layout
        layout.setContentsMargins(0, 0, 0, 0)
        # Setting the widget layout
        self.setLayout(layout)

    def setMargins(self, left, top, right, bottom):
        """Setting the margins for the widget layout

        :param left: Value for the margin on the left
        :type left: Integer

        :param top: Value for the margin on the top
        :type top: Integer

        :param right: Value for the margin on the right
        :type right: Integer

        :param bottom: Value for the margin on the bottom
        :type bottom: Integer

        :returns: None
        :rtype: None
        """
        self.layout().setContentsMargins(left, top, right, bottom)

    def isEmpty(self):
        """Setting the margins for the widget layout

        :param None:
        :type None:

        :returns: Boolean value corresponding to whether the LineEdit
                  is empty or not
        :rtype: Bool
        """
        if(self.edit.text() == ""):
            return True
        else:
            return False

    def getValue(self):
        """Returns the value of the LabelEdit as String

        :returns: The value of the LabelEdit
        :rtype: String
        """
        return self.edit.text()

    def getLabel(self):
        """Returns the text on the label

        :returns: The label text of the LabelEdit
        :rtype: String
        """
        return self.label.text()
